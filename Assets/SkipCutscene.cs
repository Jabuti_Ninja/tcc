﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkipCutscene : MonoBehaviour
{
    public GameObject cutscene;

    private void OnTriggerEnter(Collider other)
    {
        CopyObject axe = other.GetComponent<CopyObject>();

        if (axe && cutscene.activeInHierarchy)
            cutscene.SetActive(false);
    }
}
