﻿using UnityEngine;
using UnityEditor;
using System;

//[CustomEditor(typeof(CutscenesManager))]
public class CutsceneManagerEditor : Editor
{
    private SerializedObject cutscenes;
    private SerializedProperty cutscenesElement;

    private SerializedObject playerActor;
    private SerializedProperty playerActorElement;

    private SerializedObject tutorialManager;
    private SerializedProperty tutorialManagerElement;

    void OnEnable()
    {
        cutscenes = new SerializedObject(target);
        playerActor = new SerializedObject(target);
        tutorialManager = new SerializedObject(target);
    }

    override public void OnInspectorGUI()
    {

        DrawDefaultInspector();

        var cutsceneManager = target as CutscenesManager;

        cutsceneManager.debug = EditorGUILayout.Toggle("Debug", cutsceneManager.debug);

        using (var group = new EditorGUILayout.FadeGroupScope(Convert.ToSingle(cutsceneManager.debug)))
        {
            if (group.visible == true)
            {
                EditorGUI.indentLevel++;
                cutsceneManager.debugCutscene = (CutsceneType)EditorGUILayout.EnumPopup("Cutscene Type", cutsceneManager.debugCutscene);
                EditorGUI.indentLevel--;
            }
        }
    }

}
