﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutscenesManager : MonoBehaviour
{
    public Cutscene[] cutscenes;
    public PlayerActor playerActor;
    public CameraController cameraController;
    public TutorialManager tutorialManager;
    public Canvas gameCanvas;
    public GameObject stopButton;

    public static CutscenesManager Instance;

    public bool debug;
    public CutsceneType debugCutscene;

    [HideInInspector] public Cutscene currentCutscene;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        if (debug)
            TriggerCutscene(debugCutscene);
    }

    public void TriggerCutscene(CutsceneType _type)
    {
        for (int i = 0; i < cutscenes.Length; i++)
        {
            if (cutscenes[i].type == _type)
            {
                cutscenes[i].StartCutscene();
            }
                
        }
    }

    public void StopCutscene()
    {
        currentCutscene.StopCutscene();
    }

}
