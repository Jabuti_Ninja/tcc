﻿using System.Collections;
using UnityEngine;
using UnityEngine.Playables;
using ECM.Walkthrough.MovementRelativeToCamera;

public enum CutsceneType
{
    CS0,
    CS1,
    CS5,
    CS7,
    AXE_DISCOVER,
    FINAL,
    CS15,
    CS16,
    BOSS_START,
    BOSS_END
}

public enum TriggerMethod
{
    COLLIDER_IN,
    CUSTOM_TRIGGER,
}

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(PlayableDirector))] 
public class Cutscene : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private CutsceneData CutsceneData;
    public CutsceneType type;

    private BoxCollider myCollider = null;
    private PlayableDirector myTmeline = null;
    private float previousSpeed = 0;

    public GameObject startCamera;
    [HideInInspector] public bool stop;

    [SerializeField] private GameObject axe;
    [SerializeField] private SceneLoader sceneLoader;

    private void Reset()
    {
        GetComponent<BoxCollider>().isTrigger = true;
        PlayableDirector pd = GetComponent<PlayableDirector>();
        pd.playOnAwake = false;
        pd.timeUpdateMode = DirectorUpdateMode.GameTime;
        pd.extrapolationMode = DirectorWrapMode.None;
    }

    private void Start()
    {
        CutsceneData.played = false;
        myCollider = gameObject.GetComponent<BoxCollider>();
        myTmeline = gameObject.GetComponent<PlayableDirector>();

        if (CutsceneData.triggerMethod == TriggerMethod.CUSTOM_TRIGGER)
        {
            myCollider.enabled = false;
        }

        if (type == CutsceneType.CS1 && !EndGameSetterScript.passedLevel0 && !CutsceneData.played)
        {
            CutscenesManager.Instance.currentCutscene = this;
            StartCutscene();
            CutsceneData.played = true;
        }

        if (type == CutsceneType.CS15 && EndGameSetterScript.passedLevel0 && !CutsceneData.played)
        {
            CutscenesManager.Instance.currentCutscene = this;
            StartCutscene();
            CutsceneData.played = true;
        }

        if (type == CutsceneType.CS7 && !CutsceneData.played && PlayerPrefs.GetInt("cs7") <= 0)
        {
            CutscenesManager.Instance.currentCutscene = this;
            StartCutscene();
            PlayerPrefs.SetInt("cs7", 1);
            CutsceneData.played = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerActor playerActor = other.GetComponent<PlayerActor>();

        //para nao tocar a cs16 antes de passar pelo level0
        if (playerActor && type != CutsceneType.CS16)
        {
            myCollider.enabled = false;
            CutscenesManager.Instance.currentCutscene = this;
            StartCutscene();
        }
        else if (playerActor && EndGameSetterScript.passedLevel0)   //toca a cs16
        {
            myCollider.enabled = false;
            CutscenesManager.Instance.currentCutscene = this;
            StartCutscene();
        }
       
        
        if (playerActor && type == CutsceneType.BOSS_START)
        {
            LevelMusicSetter.battleMusicCount++;
        }
    }

    // Caso for chamada em outra parte do código, chamar apenas uma vez
    public void StartCutscene()
    {
        stop = false;
        StartCoroutine(CutsceneSequence());
    }

    private IEnumerator CutsceneSequence()
    {
        FadeOut.Instance.Enabled(true);

        CustomActionOnEnable();

        if (CutsceneData.blocksPlayer)
            BlockPlayerActions();

        yield return new WaitForSeconds(1f);

        if (CutsceneData.hidePlayer)
            HidePlayer();

        if (CutsceneData.hideInterface)
            CutscenesManager.Instance.gameCanvas.enabled = false;

        if (startCamera != null)
            startCamera.SetActive(true);

        yield return new WaitForSeconds(1f);

        FadeOut.Instance.Enabled(false);

        myTmeline.Play();

        while (myTmeline.state == PlayState.Playing)
        {
            if (stop)
            {
                myTmeline.Pause();
                FadeOut.Instance.Enabled(true);
                yield return new WaitForSeconds(1f);
                myTmeline.Stop();
                FadeOut.Instance.Enabled(false);
            }

            yield return new WaitForEndOfFrame();
        }

        if (CutsceneData.blocksPlayer)
            ReleasePlayerActions();

        if (CutsceneData.hidePlayer)
            ShowPlayer();

        if (CutsceneData.hideInterface)
            CutscenesManager.Instance.gameCanvas.enabled = true;

        CustomActionOnDisable();

        gameObject.SetActive(false);


    }

    public void StopCutscene()
    {
        stop = true;
    }

    #region CUSTOM_METHODS

    private void BlockPlayerActions()
    {
        CutscenesManager.Instance.playerActor.released = false;
        CutscenesManager.Instance.playerActor.controller.released = false;
        previousSpeed = CutscenesManager.Instance.playerActor.controller.speed;
        CutscenesManager.Instance.playerActor.controller.speed = 0;
        CutscenesManager.Instance.cameraController.canRotate = false;
        CutscenesManager.Instance.playerActor.canDash = false;
    }

    private void ReleasePlayerActions()
    {
        if (type != CutsceneType.CS1)
        {
            CutscenesManager.Instance.playerActor.released = true;
            CutscenesManager.Instance.playerActor.copyObject.gameObject.SetActive(true);
            if (axe)
                axe.SetActive(false);

        }
        CutscenesManager.Instance.playerActor.controller.released = true;
        CutscenesManager.Instance.playerActor.GetComponent<MyCharacterController>().speed = previousSpeed;
        CutscenesManager.Instance.cameraController.canRotate = true;
        CutscenesManager.Instance.playerActor.canDash = true;

    }

    private void HidePlayer()
    {
        SkinnedMeshRenderer playerVisual = CutscenesManager.Instance.playerActor.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        playerVisual.enabled = false;
        GameObject axe = CutscenesManager.Instance.playerActor.copyObject.axeModel.gameObject;
        axe.SetActive(false);
    }

    private void ShowPlayer()
    {
        SkinnedMeshRenderer playerVisual = CutscenesManager.Instance.playerActor.gameObject.GetComponentInChildren<SkinnedMeshRenderer>();
        playerVisual.enabled = true;
        GameObject axe = CutscenesManager.Instance.playerActor.copyObject.axeModel.gameObject;
        axe.SetActive(true);
    }

    private void CustomActionOnEnable()
    {
        switch (type)
        {
            case CutsceneType.AXE_DISCOVER:
                CutscenesManager.Instance.tutorialManager.HidePanel(0);
                break;
            case CutsceneType.CS1:
                FadeOut.Instance.startBlack.SetActive(true);
                break;
            case CutsceneType.CS7:
                FadeOut.Instance.startBlack.SetActive(true);
                break;
            case CutsceneType.CS15:
                FadeOut.Instance.startBlack.SetActive(true);
                break;
        }
    }

    private void CustomActionOnDisable()
    {
        switch (type)
        {
            case CutsceneType.AXE_DISCOVER:
                CutscenesManager.Instance.tutorialManager.ShowPanel(1);
                break;
            case CutsceneType.CS16:
                sceneLoader.LoadLevel(0);
                break;
        }
    }

    #endregion

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("cs7", 0);

    }
}
