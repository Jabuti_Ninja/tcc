﻿using UnityEngine;

[CreateAssetMenu(fileName = "Cutscene Data", menuName = "Datas/Cutscene Data")]
public class CutsceneData : ScriptableObject
{
    public TriggerMethod triggerMethod;
    public bool blocksPlayer;
    public bool hidePlayer;
    public bool played;
    public bool hideInterface;
}
