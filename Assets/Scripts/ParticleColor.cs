﻿using UnityEngine;

public class ParticleColor : MonoBehaviour
{
    [SerializeField] private ParticleSystem[] particles;
    [SerializeField] private Light[] lights;
    [SerializeField] private float simSpeed = 2;

    private void Start()
    {
        foreach (var particle in particles)
        {
            SetVFXSpeed(particle, simSpeed);
        }
    }

    public void PlayVFXColor(Color color)
    {
        foreach (var particle in particles)
        {
            SetVFXcolor(particle, color);
            particle.Play();
        }

        if(lights.Length > 0)
        {
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].color = color;
            }
        }
    }

    private void SetVFXSpeed(ParticleSystem vfx, float speed)
    {
        var main = vfx.main;
        main.simulationSpeed = speed;
    }

    public void SetVFXcolor(ParticleSystem vfx, Color color)
    {
        vfx.gameObject.GetComponent<Renderer>().material.SetColor("_EmissionColorCustom", color);
    }
}
