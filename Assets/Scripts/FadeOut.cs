﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOut : MonoBehaviour
{
    public static FadeOut Instance;
    public Animator myAnimator;
    public GameObject startBlack;

    private void Awake()
    {
        Instance = this;
        myAnimator = gameObject.GetComponent<Animator>();
    }

    public void Enabled(bool enabled)
    {
        myAnimator.SetBool("On", enabled);
    }
}
