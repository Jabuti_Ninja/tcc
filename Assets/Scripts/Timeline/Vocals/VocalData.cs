﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public enum VocalType
{
    Generic,
    Camila
}

[CreateAssetMenu]
public class VocalData : ScriptableObject
{
    public List<VocalGroup> vocalGroups = new List<VocalGroup>();

#if UNITY_EDITOR

    public void GetVocalGroups()
    {
        vocalGroups.Clear();

        foreach (VocalType type in Enum.GetValues(typeof(VocalType)))
        {
            VocalGroup group = new VocalGroup(type);

            if (Directory.Exists("Assets/Sounds/Axe Vocals/Resources/" + type.ToString()))
            {
                group.clips = Resources.LoadAll<AudioClip>(type.ToString());
                vocalGroups.Add(group);
            }

            else
                Debug.LogError("The folder " + type.ToString() + " could not be found");
        }

        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

# endif 

}

[Serializable]
public class VocalGroup
{
    [HideInInspector] public string name;
    public VocalType vocalType;
    public AudioClip[] clips;

    public VocalGroup(VocalType type)
    {
        name = type.ToString();
        vocalType = type;
        clips = null;
    }
}
