﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackBindingType(typeof(SubtitlesCanvas))]
[TrackClipType(typeof(SubtitleClip))]
public class SubtitleTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        foreach (TimelineClip clip in m_Clips)
        {
            SubtitleClip subtitleClip = clip.asset as SubtitleClip;
            clip.displayName = subtitleClip.subtitleText;
        }

        return ScriptPlayable<SubtitleTrackMixer>.Create(graph, inputCount);


    }
}
