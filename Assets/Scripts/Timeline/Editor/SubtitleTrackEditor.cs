﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Timeline;
using UnityEngine;
using UnityEngine.Timeline;

[CustomTimelineEditor(typeof(SubtitleTrack))]
public class SubtitleTrackEditor : TrackEditor
{
    private Texture2D icon = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Textures/UI/Logo_Camila.png");

    public override TrackDrawOptions GetTrackOptions(TrackAsset track, Object binding)
    {
        var options = base.GetTrackOptions(track, binding);

        options.icon = icon;
        options.trackColor = Color.cyan;

        return options;
    }
}
