﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(VocalData))]
public class VocalDataEditor : Editor
{
    [MenuItem("Tools/Vocal Groups")]
    static void Open()
    {
        AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath("Assets/Scripts/Timeline/Vocals/Vocal Groups.asset", typeof(VocalData)));
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        VocalData data = target as VocalData;

        if (GUILayout.Button("Get Audios"))
            data.GetVocalGroups();
    }
}
