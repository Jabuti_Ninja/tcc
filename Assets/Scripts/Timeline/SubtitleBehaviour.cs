﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SubtitleBehaviour : PlayableBehaviour
{
    public string subtitleText;
    public int characterID;
    public Sprite characterSprite;
    public bool playVocal;
    public VocalType vocalType;
    public float vocalVolume;
}
