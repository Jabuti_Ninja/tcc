﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SubtitleTrackMixer : PlayableBehaviour
{
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        SubtitlesCanvas canvas = playerData as SubtitlesCanvas;
        string currentText = string.Empty;
        int currentID = 0;
        Sprite currentCharacter = null;
        float currentAlpha = 0f;
        bool playVocal = false;
        VocalType vocalType = VocalType.Generic;
        float vocalVolume = 0;

        if (!canvas)
            return;

        int inputCount = playable.GetInputCount();

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);

            if (inputWeight > 0f)
            {
                ScriptPlayable<SubtitleBehaviour> inputPlayable = (ScriptPlayable<SubtitleBehaviour>)playable.GetInput(i);
                SubtitleBehaviour input = inputPlayable.GetBehaviour();

                currentText = input.subtitleText;
                currentAlpha = inputWeight;
                currentID = input.characterID;
                currentCharacter = input.characterSprite;
                playVocal = input.playVocal;
                vocalType = input.vocalType;
                vocalVolume = input.vocalVolume;
            }

        }

        #region SUBTITLES

        Color color = new Color(1, 1, 1, currentAlpha);

        canvas.text.text = currentText;
        canvas.text.color = color;
        
        switch (currentID)
        {
            case 0:
                canvas.characters[0].color = Color.clear;
                canvas.characters[1].color = Color.clear;
                break;
            case 1:
                canvas.characters[0].color = Color.white;
                canvas.characters[1].color = Color.clear;
                canvas.characters[currentID - 1].sprite = currentCharacter;
                break;
            case 2:
                canvas.characters[0].color = Color.clear;
                canvas.characters[1].color = Color.white;
                canvas.characters[currentID - 1].sprite = currentCharacter;
                break;
        }

        canvas.charactersCanvas.alpha = currentAlpha;

        #endregion

        if (playVocal)
        {
            if(!canvas.audioSource.isPlaying)
                canvas.PlayVocal(vocalType);

            canvas.audioSource.volume = currentAlpha * vocalVolume;
        }

        else
        {
            if (canvas.audioSource.isPlaying)
            {
                canvas.audioSource.Stop();
            }
        }

    }
}
