﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class SubtitleClip : PlayableAsset
{
    [Header("Subtitles")]
    [TextArea] public string subtitleText;
    [Range(0, 2)] public int characterID;
    public Sprite characterSprite;
    [Header("Vocals")]
    public bool playVocal;
    public VocalType vocalType;
    [Range(0, 1)] public float vocalVolume;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<SubtitleBehaviour>.Create(graph);

        SubtitleBehaviour subtitleBehaviour = playable.GetBehaviour();
        subtitleBehaviour.subtitleText = subtitleText;
        subtitleBehaviour.characterID = characterID;
        subtitleBehaviour.characterSprite = characterSprite;
        subtitleBehaviour.playVocal = playVocal;
        subtitleBehaviour.vocalType = vocalType;
        subtitleBehaviour.vocalVolume = vocalVolume;

        return playable;
    }

}
