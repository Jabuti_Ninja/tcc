﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SubtitlesCanvas : MonoBehaviour
{
    public Image[] characters;
    public TextMeshProUGUI text;
    public CanvasGroup charactersCanvas;
    public AudioSource audioSource;
    public VocalData vocalData;

    VocalGroup currentGroup;

    public void PlayVocal(VocalType vocalType)
    {
        foreach (VocalGroup group in vocalData.vocalGroups)
        {
            if (group.vocalType == vocalType)
            {
                currentGroup = group;
                break;
            }
        }

        if (currentGroup.clips.Length == 0)
            return;

        audioSource.clip = currentGroup.clips[Random.Range(0, currentGroup.clips.Length - 1)];
        audioSource.Play();

    }
    
}
