﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropdownManager : MonoBehaviour
{
    [SerializeField] private HUDSettings settings;

    public TMPro.TMP_Dropdown dropdown;
    public DropdownType myType;

    public void GetDropdownValue()
    {
        settings.ChangeDropddownValue(dropdown.value, myType);

        if(myType != DropdownType.QUALITY)
        {
            RenderManager.Instance.UpdateRender();

            if(!HUDSettings.isCustom && HUDSettings.validCustom)
                settings.SetCustom();
        }
            
    }

}
