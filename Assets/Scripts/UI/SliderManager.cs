﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderManager : MonoBehaviour
{
    [SerializeField] private HUDSettings settings;
    public Slider mySlider;

    public void GetSliderValue()
    {
        settings.ChangeSliderValues(mySlider.value);
        RenderManager.Instance.UpdateRender();
    }
}
