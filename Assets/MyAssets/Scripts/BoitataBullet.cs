﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoitataBullet : MonoBehaviour
{

    private Collider col;
    [SerializeField] private float disableTimer;
    [SerializeField] private float pushForce;
    [SerializeField] private ParticleSystem pSystem;
    public int damage;

    private List<StateController> enemiesDamaged = new List<StateController>();

    // Start is called before the first frame update
    void Awake()
    {
        col = GetComponent<Collider>();
        col.enabled = true;
        OnEnable();
    }

    private void OnEnable()
    {
        col.enabled = true;
        var mainParticle = pSystem.main;
        mainParticle.loop = true;
        StartCoroutine(SelfDisable());
    }

    private void OnDisable()
    {
        enemiesDamaged.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        var enemy = other.GetComponent<StateController>();
        if(!enemy)
            enemy = other.GetComponentInParent<StateController>();
        if(!enemiesDamaged.Contains(enemy))
        {
            if (enemy)
            {
                enemy.TakeDamage(damage);
                Vector3 dir = (enemy.transform.position - transform.position).normalized;
                enemy.rb.AddForce(dir * pushForce, ForceMode.Impulse);
                enemy.Hitstun();

                enemiesDamaged.Add(enemy);
            }
        }

    }

    public IEnumerator SelfDisable()
    {
        yield return new WaitForSeconds(disableTimer);
        var mainParticle = pSystem.main;
        mainParticle.loop = false;
        col.enabled = false;
        yield return new WaitForSeconds(1.5f);
        
        ObjectPooler.SharedInstance.PoolDestroy(gameObject);
        yield return null;
    }
}
