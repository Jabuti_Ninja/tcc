﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPortalScript : Puzzle
{
    private bool isUnlocked;
    public int portalID;
    public int levelToGo;
    public SceneLoader sceneLoader;
    public Animator metroAnimator;
    public Animator energyWall;
    public float dissipateTime;
    [SerializeField]private GameObject cutscene;
    public override void Act()
    {
        if (!energyWall)
            Open();
        else
            DissipateEnergyWall();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerActor>() && isUnlocked)
        {
            sceneLoader.LoadLevel(levelToGo);
            EndGameSetterScript.passedLevel0 = true;
        }
    }

    public void Open()
    {
        metroAnimator.SetTrigger("open");
        FMODUnity.RuntimeManager.PlayOneShot("event:/Puzzle/Metro_opening", transform.position);
    }

    public void DissipateEnergyWall()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 3)
            energyWall.SetTrigger("destroy");
        else
            energyWall.SetTrigger("destroy1");
        if (!isUnlocked)
            FMODUnity.RuntimeManager.PlayOneShot("event:/Puzzle/Portal_opening", transform.position);

        if (cutscene)
            cutscene.SetActive(false);

        StartCoroutine(WaitDissipate());
    }

    IEnumerator WaitDissipate()
    {
        //yield return new WaitUntil(() => !energyWall.GetCurrentAnimatorStateInfo(0).IsName("Energy_Wall_Destroy_Level_0"));
        yield return new WaitForSeconds(dissipateTime);
        energyWall.gameObject.SetActive(false);
        if (!isUnlocked)
            Open();
        isUnlocked = true;
    }
}
