﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBulletLauncher : MonoBehaviour
{
    private Rigidbody rb;
    public float force = 1;

    [SerializeField] private bool useAddForce;




    private void Awake()
    {
        rb = GetComponent<Rigidbody>();

    }


    private void OnEnable()
    {
        rb.isKinematic = false;
        rb.velocity = transform.forward * force;

    }

    private void OnDisable()
    {
        rb.isKinematic = true;
        rb.velocity = Vector3.zero;
    }

    private void Update()
    {
        if(gameObject.activeInHierarchy)
        {
            GoFoward();
        }
    }

    void GoFoward()
    {
        if (!useAddForce)
            rb.velocity = transform.forward * force;
        else
            rb.AddForce(force * transform.forward * Time.deltaTime, ForceMode.Impulse);
    }



    private void SelfDestruct()
    {
        ObjectPooler.SharedInstance.PoolDestroy(gameObject);
    }
}
