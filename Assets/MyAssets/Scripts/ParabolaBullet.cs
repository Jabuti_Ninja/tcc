﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ParabolaBullet : MonoBehaviour
{
    private Rigidbody rb;

    [Header("Parabola Settings")]
    public Transform destination;
    public float height = 1;
    public float duration = 1;
    public Tween parabolaTween;
    public EnemyBullet explosion;

    [Header("General Settings")]
    public float baseLifeTime = 5f;
    public int damage = 1;
    [SerializeField] private GameObject targetFeedback;
    [SerializeField] private float targetFeedbackOffset;


    void Start()
    {
        //rb = GetComponent<Rigidbody>();
        OnEnable();
    }

    private void OnEnable()
    {
        if (destination)
        {
            targetFeedback.SetActive(true);
            parabolaTween = transform.DOJump(destination.position, height, 1, duration, false);
            targetFeedback.transform.parent = null;
            targetFeedback.transform.position = destination.position - new Vector3(0, targetFeedbackOffset, 0); ;
        }
        explosion.gameObject.SetActive(false);
        StartCoroutine(AutoSelfDestruct());
    }

    private void OnTriggerEnter(Collider other)
    {

        //if (collision.gameObject.CompareTag("Player"))
        //{
        //    var hitPoints = collision.gameObject.GetComponent<HitPoints>();
        //    if (hitPoints)
        //        hitPoints.Damage(damage);
        //}
        if (other.tag != "Enemy")
            SelfDestruct();
    }
 
    private IEnumerator AutoSelfDestruct()
    {
        yield return new WaitForSeconds(baseLifeTime);
        SelfDestruct();
        yield return null;
    }

    private void SelfDestruct()
    {
        StopAllCoroutines();
        explosion.gameObject.SetActive(true);
        explosion.damage = damage;
        explosion.originalParent = transform;
        explosion.transform.position = transform.position;
        explosion.transform.parent = null;
        ObjectPooler.SharedInstance.PoolDestroy(gameObject);
        targetFeedback.SetActive(false);
    }
}
