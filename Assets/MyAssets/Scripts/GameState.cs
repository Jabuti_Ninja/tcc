﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameState : MonoBehaviour
{

    public static bool gamePaused = false;
    [SerializeField] private PlayerActor player;
    [SerializeField] private GameObject deathPanel;
    [SerializeField] private float normalTimeScale = 1;
    [SerializeField] private GameObject pausePanel;

    [SerializeField] private GameObject endPanel;
    [SerializeField] private GameObject titlePanel;

    [SerializeField] private bool endTimer = false;
    [SerializeField] private float timeToEnd = 30;
    public Text timerText;

    public static Vector3 playerSpawn;
    public Vector3 level0StartPos;
    public Vector3 level1StartPos;
    public Vector3 bossLevelPos = new Vector3(0, 0, 0);

    public static GameState Instance { get; private set; }

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else Destroy(gameObject);

    }
    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 3)
            playerSpawn = level0StartPos;
        else if (SceneManager.GetActiveScene().buildIndex == 4 && !PlayerActor.died)
            playerSpawn = level1StartPos;

        else if (SceneManager.GetActiveScene().buildIndex == 5)
            playerSpawn = bossLevelPos;


        if (SceneManager.GetActiveScene().buildIndex == 0 || SceneManager.GetActiveScene().buildIndex == 1)
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = normalTimeScale;
            Cursor.visible = true;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Time.timeScale = normalTimeScale;
        }
    }

    void Update()
    {
        CheckEndTimer();
    }

    private void CheckEndTimer()
    {
        if (endTimer)
        {
            if (Time.time >= timeToEnd)
            {
                Debug.LogWarning("TIMER ENDED");
                EndGame(false);
            }
            //else
            //    Debug.Log("base Timer: " + Time.time);
        }
    }

    public void CheckPauseInput()
    {
        if (SceneManager.GetActiveScene().buildIndex > 1 && !deathPanel.activeInHierarchy)
        {
            if (!gamePaused && !deathPanel.activeInHierarchy)
            {
                PauseGame();
            }
            else
                ResumeGame();
        }
    }

    public void PauseGame()
    {
        gamePaused = true;
        pausePanel.SetActive(true);
        //reseta os paineis do pause
        pausePanel.transform.GetChild(0).gameObject.SetActive(true);
        pausePanel.transform.GetChild(1).gameObject.SetActive(false);
        pausePanel.transform.GetChild(2).gameObject.SetActive(false);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;

    }

    public void ResumeGame()
    {
        gamePaused = false;
        pausePanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = normalTimeScale;

    }

    public void EndGame(bool victory)
    {
        gamePaused = true;
        endPanel.SetActive(true);

        if (victory)
            titlePanel.SetActive(true);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        StartCoroutine(DelayedTimescale());
    }

    private IEnumerator DelayedTimescale()
    {
        yield return new WaitForSeconds(2f);
        Time.timeScale = 0;
    }

    public void PlayerDead()
    {
        deathPanel.SetActive(true);
        player.released = false;
        player.controller.released = false;
        //gamePaused = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0;
    }
}

