﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDeathTrigger : MonoBehaviour
{

    [SerializeField] private StateController boss;
    //[SerializeField] private GameObject levelExit;
    [SerializeField] private Cutscene finalCutscene;
    public float delay = 1;

    private Coroutine coroutine;

    void Update()
    {
        if(boss.dead && coroutine == null)
        {
            LevelMusicSetter.battleMusicCount = 0;
            coroutine = StartCoroutine(triggerLevelExit());
        }
    }

    private IEnumerator triggerLevelExit()
    {
        yield return new WaitForSeconds(delay);
        CutscenesManager.Instance.currentCutscene = finalCutscene;
        finalCutscene.StartCutscene();
    }
}
