﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActor : Actor
{

    [Range (1, 99)]public int powerId;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();

        if(!isDead)
        {
            if (hitPoints.currentPoints <= 0)
            {                
                isDead = true;
            }
        }

    }

}
