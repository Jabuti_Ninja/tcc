﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySummoner : MonoBehaviour
{
    public ObjectPooler[] enemyPooler;
    public Transform[] enemySpawnPos;

    public ObjectPooler spawnVFXPooler;

    [HideInInspector] public List<StateController> enemiesSummoned;


    public StateController SummonEnemy(int index)
    {
        spawnVFXPooler.PoolInstantiate(enemySpawnPos[index].position, enemySpawnPos[index].rotation).GetComponent<ParticleSystem>().Play();
        return enemyPooler[index].PoolInstantiate(enemySpawnPos[index].position, enemySpawnPos[index].rotation).GetComponent<StateController>();        
    }
}
