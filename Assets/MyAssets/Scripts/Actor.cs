﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Actor : MonoBehaviour
{
    
    [HideInInspector]public HitPoints hitPoints;
    [HideInInspector] public Ragdoller ragdoller;
    [HideInInspector] public Rigidbody rb;
    [HideInInspector]public ParticleShooter pShooter;
    [HideInInspector] public AudioSource audioSource;
    public ParticleSystem weaponParticle;
    public List<ParticleSystem> secondaryWeaponParticles;
    public SoundBank soundBank;



    [HideInInspector] public bool isDead;

    protected virtual void Awake()
    {
        pShooter = GetComponent<ParticleShooter>();
        hitPoints = GetComponent<HitPoints>();
        ragdoller = GetComponent<Ragdoller>();
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();

    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        
    }

    public virtual void ParticleShoot()
    {
        weaponParticle.Play();
    }

    public void TakeDamage(int damage)
    {
        hitPoints.Damage(damage);
    }
}
