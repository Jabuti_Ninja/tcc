﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ragdoller : MonoBehaviour
{
    private Collider[] ragColliders;
    private Animator animator;

    void Start()
    {
        ragColliders = GetComponentsInChildren<Collider>();
        animator = GetComponentInChildren<Animator>();
    }

  // IEnumerator Test()
  // {
  //     yield return new WaitForSeconds(3f);
  //     DoRagdoll(true);
  //     yield return new WaitForSeconds(3f);
  //     DoRagdoll(false);
  //     yield return null;
  //
  // }

    public void DoRagdoll(bool isRagdoll)
    {
        animator.enabled = !isRagdoll;
        foreach (Collider item in ragColliders)
        {            
            if(item.attachedRigidbody)
            {
                item.attachedRigidbody.useGravity = isRagdoll;
                item.attachedRigidbody.velocity = Vector3.zero;
            }            
        }
    }

    public void TimedRagdoll(float duration)
    {
        StartCoroutine(DoTimedRagdoll(duration));
    }

    IEnumerator DoTimedRagdoll(float duration)
    {
        DoRagdoll(true);
        yield return new WaitForSeconds(duration);
        DoRagdoll(false);
        yield return null;
    }
}
