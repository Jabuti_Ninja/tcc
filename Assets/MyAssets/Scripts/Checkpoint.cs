﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{

    [SerializeField] private GameObject checkPointFeedback;
    private bool sawFeedback;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameState.playerSpawn = transform.position;
            if (!sawFeedback)
            {
                checkPointFeedback.SetActive(true);
                StartCoroutine(SelfDisable());
                sawFeedback = true;
            }
        }
    }

    IEnumerator SelfDisable()
    {
        yield return new WaitForSeconds(3f);
        checkPointFeedback.SetActive(false);
    }
}
