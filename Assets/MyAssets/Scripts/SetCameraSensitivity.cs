﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SetCameraSensitivity : MonoBehaviour
{
    // Start is called before the first frame update
    private Slider slider;
    private TMP_Text text;
    public CameraController cameraController;
    public bool cameraX;
    public bool cameraY;
    void Start()
    {
        slider = GetComponent<Slider>();
        text = GetComponentInChildren<TMP_Text>();
        text.text = slider.value.ToString("F1");

        if (cameraX)
        {
            slider.value = PlayerPrefs.GetFloat("sensitivityX");
            if(cameraController)
            cameraController.sensitivityX = slider.value;
        }
        if (cameraY)
        {
            slider.value = PlayerPrefs.GetFloat("sensitivityY");
            if (cameraController)
                cameraController.sensitivityY = slider.value;
        }

    }

    public void SetVarx()
    {
        if (cameraController)
            cameraController.sensitivityX = slider.value;
        PlayerPrefs.SetFloat("sensitivityX", slider.value);
        text.text = slider.value.ToString("F1");
    }

    public void SetVarY()
    {
        if (cameraController)
            cameraController.sensitivityY = slider.value;
        PlayerPrefs.SetFloat("sensitivityY", slider.value);
        text.text = slider.value.ToString("F1");
    }
}
