﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECM.Walkthrough.MovementRelativeToCamera;
using UnityEngine.Playables;



public class GraffitiScript : MonoBehaviour
{
    [SerializeField] private GameObject eFeedback;


    private MyCharacterController controller;
    private PlayerActor playerActor;
    [SerializeField] private CameraController cameraController;

    private PlayableDirector myTmeline = null;

    private float previousSpeed;


    private bool graffitiMode;
    private bool isInside;

    //bool pq o input system chama mais de uma vez, deixando interact bool mais de uma vez, oq faz com que saia e entre do modo grafite ao pressionar o botão apenas uma vez
    private bool canCheck = true;

    private void Start()
    {
        myTmeline = gameObject.GetComponent<PlayableDirector>();

    }

    private void Update()
    {
        if (isInside && controller.interact && !graffitiMode && canCheck)
        {
            EnterGraffitiMode();
        }

        if (isInside && controller.interact && graffitiMode && canCheck)
        {
            ExitGraffitiMode();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        controller = other.GetComponent<MyCharacterController>();
        playerActor = other.GetComponent<PlayerActor>();
        if (playerActor && LevelMusicSetter.battleMusicCount <= 0)
        {
            isInside = true;
            eFeedback.SetActive(true);
        }


    }

    private void OnTriggerExit(Collider other)
    {
        playerActor = other.GetComponent<PlayerActor>();

        if (playerActor)
        {
            isInside = false;
            eFeedback.SetActive(false);

        }

    }

    private void EnterGraffitiMode()
    {
        myTmeline.Play();
        eFeedback.SetActive(false);
        graffitiMode = true;
        BlockPlayerActions();
        StartCoroutine(InputDelay());
    }

    private void ExitGraffitiMode()
    {
        myTmeline.Stop();
        if (isInside)
            eFeedback.SetActive(true);

        graffitiMode = false;
        ReleasePlayerActions();
        StartCoroutine(InputDelay());

    }

    private void BlockPlayerActions()
    {
        playerActor.controller.released = false;
        previousSpeed = playerActor.controller.speed;
        playerActor.controller.speed = 0;
        playerActor.canDash = false;
        cameraController.canRotate = false;
    }

    public void ReleasePlayerActions()
    {

        playerActor.controller.released = true;
        playerActor.controller.speed = previousSpeed;
        cameraController.canRotate = true;
        playerActor.canDash = true;

    }


    IEnumerator InputDelay()
    {
        canCheck = false;
        yield return new WaitForSeconds(1f);
        canCheck = true;


    }
}
