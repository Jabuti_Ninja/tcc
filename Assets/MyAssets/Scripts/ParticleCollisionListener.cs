﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollisionListener : MonoBehaviour
{
    private ParticleSystem part;
    private List<ParticleCollisionEvent> collisionEvents;
    public int damage = 1;
    public bool doRagdoll = false;
    public float ragdollForce = 30f;
    public int powerId;
    [HideInInspector]public GameObject hitMark;

    public bool hasDmgInterval = false;
    public float baseDmgInterval = 0;
    private bool canDamage = true;
    private float dmgTimer = 0;

    void Start()
    {
        int.TryParse(name.Substring(0, 2), out powerId);

        
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    private void Update()
    {
        if(hasDmgInterval)
            canDamage = CheckDmgInterval();
    }

    private bool CheckDmgInterval()
    {
        dmgTimer += Time.deltaTime;

        if (dmgTimer >= baseDmgInterval)
        {
            dmgTimer = 0;
            return true;
        }
        else return false;
    }

    private void OnParticleCollision(GameObject other)
    {        
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents); //Obsolete?
        for (int i = 0; i < numCollisionEvents; i++)
        {
            var actor = other.GetComponentInParent<Actor>();
            if (actor == null)
                actor = other.GetComponent<Actor>();
            if (actor == null)
                return;
            if (hasDmgInterval && !canDamage)
                return;
            if((doRagdoll && actor.ragdoller) /*|| actor.isDead && actor.ragdoller*/)
            {
                actor.ragdoller.DoRagdoll(true);
                other.GetComponent<Rigidbody>().AddForce(collisionEvents[i].intersection * ragdollForce, ForceMode.Impulse);
            }

            //se for player                          //se o powerpoints tiver ativo ele da dano la
            if (actor.GetComponent<PowerPoints>() && actor.GetComponent<PowerPoints>().powerBar.activeInHierarchy)
            {
                actor.GetComponent<PowerPoints>().PowerBarDamage(damage);
            }
            else    // da dano no hitpoints (player e inimigos)
            {
                if (actor.GetComponent<PowerPoints>())//checa se tem o powerpoints pq se sim é o player
                {      //da dano no hitpoints do player
                    actor.hitPoints.Damage(damage);
                    actor.hitPoints.Shake(.1f, 1, 1);
                }
                else
                {
                    actor.hitPoints.Damage(damage);         //ativa o hitmark e da dano se for inimigo tomando dano
                    if (hitMark)
                        hitMark.SetActive(true);
                }

            }

        }
    }

}
