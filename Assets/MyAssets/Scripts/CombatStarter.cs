﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatStarter : MonoBehaviour
{
    public StateController[] enemies;
    public GameObject[] walls;
    public bool bossArena;
    public int enemiesAlive;
    private bool playedFinalCutscene;

    private bool closed;
    private bool started;
    private void Awake()
    {
        if (enemies.Length == 0)
            enemies = GetComponentsInChildren<StateController>();
        enemiesAlive = enemies.Length;
    }

    private void OnTriggerEnter(Collider other)
    {
        var axe = other.GetComponent<CopyObject>();

        if (other.GetComponent<PlayerActor>() || axe)
        {
            StartEnemiesCombat();
            if (bossArena && !closed)
                CloseArena();
        }
    }

    private void StartEnemiesCombat()
    {

        if (!started)
        {
            foreach (var enemy in enemies)
            {

                enemy.StartCombat();
            }
            LevelMusicSetter.battleMusicCount++;
            started = true;
        }

    }

    public void CheckOpenArena()
    {
        enemiesAlive--;
        if (enemiesAlive <= 0)
        {
            OpenArena();
        }
    }


    private void CloseArena()
    {
        for (int i = 0; i < walls.Length; i++)
        {
            walls[i].SetActive(true);
        }
        closed = true;
    }

    private void OpenArena()
    {
        for (int i = 0; i < walls.Length; i++)
        {
            walls[i].SetActive(false);
        }
        LevelMusicSetter.battleMusicCount--;

    }

}
