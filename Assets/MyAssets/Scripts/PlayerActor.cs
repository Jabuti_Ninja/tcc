﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.InputSystem;
using ECM.Walkthrough.MovementRelativeToCamera;
using SensorToolkit;
using DG.Tweening;
using Ludiq;
using UnityEngine.Animations.Rigging;
using System;

public class PlayerActor : Actor
{
    [HideInInspector] public MyCharacterController controller;
    [HideInInspector] public CopyObject copyObject;
    [HideInInspector] public PlaySFX playSFX;

    [HideInInspector] public ProjectileShooting projectileShooting;
    public Transform projectileShotPos;

    public List<Power> powerList;
    public List<GameObject> powerVFXList;

    public PowerPoints powerPoints;
    public Power currentPower;
    public Transform shootPos;




    // Bool adicionada para fazer a cutscene
    public bool released = true;
    public GameObject hitMark;
    public GameObject rightArmIKController;
    public TutorialManager tutorialManager;

    //variaveis pra setar um delay pro player usar os poderes
    private float nextFireTime = 0;
    [SerializeField] private bool canFire = true;

    private float secondaryNextFireTime = 0;
    [SerializeField] private bool secondaryCanFire = true;

    [Range(0, 1)] private float sfxLerpRange;
    [HideInInspector] public Coroutine fadeCoroutine;

    public static PlayerActor playerInstance;

    public delegate void OnPowerReset();
    public event OnPowerReset onPowerReset;

    public static bool died;

    [HideInInspector] public bool canDash = true;
    [SerializeField] private float dashCoolDown = 1f;
    [SerializeField] private float dashPower;
    [SerializeField] private GameObject dashEnergyTrail;

    protected override void Awake()
    {
        base.Awake();



        //Carrega os vfx dos poderes da pasta
        var particles = Resources.LoadAll("PowersVFX", typeof(GameObject)).Cast<GameObject>();
        foreach (var go in particles)
        {
            powerVFXList.Add(Instantiate(go, transform));
        }

        //Carrega os poderes da pasta
        var powers = Resources.LoadAll("ScriptableObjects/Powers", typeof(Power)).Cast<Power>();
        foreach (var go in powers)
        {
            int.TryParse(go.name.Substring(0, 2), out go.id);
            powerList.Add(go);
        }

        //Pega as refs
        powerPoints = GetComponent<PowerPoints>();
        controller = GetComponent<MyCharacterController>();
        copyObject = GetComponentInChildren<CopyObject>();
        projectileShooting = GetComponent<ProjectileShooting>();
        playSFX = GetComponent<PlaySFX>();

        //copyObject.soundBank = controller.soundBank;
        //soundBank = controller.soundBank;

        copyObject.hitMark = hitMark;
        copyObject.tutorialManager = tutorialManager;

        playerInstance = this;


    }

    protected override void Start()
    {
        base.Start();
        isDead = false;

        currentPower = powerList.Find(x => x.id == 0); //Seta o copy power como current power
        powerPoints.currentPoints = currentPower.powerAmmo;
        if (!released)
        {
            copyObject.gameObject.SetActive(false);
        }

        if (GameState.playerSpawn != Vector3.zero && UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 4 && died)
        {
            transform.position = GameState.playerSpawn;
            released = true;
            controller.released = true;
            controller.animator.SetTrigger("respawn");
            playSFX.PlaySoundFX("event:/Player/Player_spawn", 0f);
            copyObject.gameObject.SetActive(true);
        }


    }

    private void OnEnable()
    {
        copyObject.onPowerChange += ChangePower;
        copyObject.onPowerChange += FillAmmo;
        copyObject.onPowerChange += UpdateWeaponParticle;
        copyObject.onHandReturn += Shake;
        copyObject.onHandReturn += PlayReturnToHandAnim;
        copyObject.onEnemyHit += ChangeVFXColor;
        controller.onJump += PlayJumpSFX;
        controller.onWalk += PlayFootStepSFX;
        hitPoints.onDamage += PlayHitSFX;

    }

    private void OnDisable()
    {
        copyObject.onPowerChange -= ChangePower;
        copyObject.onPowerChange -= FillAmmo;
        copyObject.onPowerChange -= UpdateWeaponParticle;
        copyObject.onHandReturn -= Shake;
        copyObject.onHandReturn -= PlayReturnToHandAnim;
        copyObject.onEnemyHit -= ChangeVFXColor;
        controller.onJump -= PlayJumpSFX;
        controller.onWalk -= PlayFootStepSFX;
        hitPoints.onDamage -= PlayHitSFX;

    }

    protected override void Update()
    {
        base.Update();
        if (copyObject.copyPowerID != 0 && powerPoints.currentPoints <= 0)
        {
            ResetToCopyPower();
        }

        canFire = CheckTimer(nextFireTime);
        secondaryCanFire = CheckTimer(secondaryNextFireTime);

        //chamar isso em evento
        if (currentPower.id != 0)
        {
            SetIkWeight(rightArmIKController, .75f);
        }
        else if (currentPower.id == 0 && copyObject.state == CopyObject.State.InHand)
            SetIkWeight(rightArmIKController, 0);

        if (hitPoints.currentPoints <= 0 && !isDead)
        {
            StartCoroutine(Die());
            isDead = true;
        }

        if (weaponParticle)
        {
            AimToReticleTarget(weaponParticle.transform);
        }

        if (isDead)
        {
            rb.constraints = RigidbodyConstraints.FreezePositionX;
            rb.constraints = RigidbodyConstraints.FreezePositionZ;
        }

        AimToReticleTarget(projectileShotPos);

    }

    public bool CheckTimer(float timerToCheck)
    {
        if (Time.time >= timerToCheck)
        {
            return true;
        }
        else return false;
    }

    public void ResetTimer(float timeToAdd)
    {
        nextFireTime = Time.time + timeToAdd;
    }

    public void ResetSecondaryTimer(float timeToAdd)
    {
        secondaryNextFireTime = Time.time + timeToAdd;
    }


    public void ExecutePower(InputAction.CallbackContext context)
    {
        // Bool adicionada para fazer a cutscene
        if (context.performed && released && Time.timeScale == 1)
        {
            if (tutorialManager && tutorialManager.SawPanel(4) && !tutorialManager.SawPanel(5))
            {
                tutorialManager.HidePanel(4);
                tutorialManager.ShowPanel(5);
            }



            if (canFire)
            {
                currentPower.ExecutePrimaryPower(this);
                ResetTimer(currentPower.fireRate);
            }


        }
        if (context.canceled && released)
        {
            currentPower.ExecutePrimaryPowerRelease(this);
        }
    }

    public void ExecuteSecondaryPower(InputAction.CallbackContext context)
    {
        if (context.performed && released)
        {
            if (secondaryCanFire)
            {
                currentPower.ExecuteSecondaryPower(this);
                ResetSecondaryTimer(currentPower.secondaryFireRate);
            }
        }
    }

    public IEnumerator ExecuteCopy(float throwDelay)
    {
        var cam = controller.mainCamera.GetComponent<RaySensor>();
        var targets = cam.DetectedObjects;
        if (copyObject.state == CopyObject.State.InHand && copyObject.copyPowerID == 0) // só joga se tiver o poder de copiar e na mao
        {
            controller.animator.SetTrigger("throw");
            SetIkWeight(rightArmIKController, 0);
            AimToReticleTarget(copyObject.throwPos);
            copyObject.Throw(CopyObject.State.GoingForward, throwDelay);
        }


        else if (copyObject.state != CopyObject.State.InHand)
        {
            copyObject.state = CopyObject.State.Returning; // se apertar o botão com o machado fora da mão
            SetIkWeight(rightArmIKController, 1f);
        }


        yield return null;
    }

    public void AimToReticleTarget(Transform objToAim)
    {
        var cam = controller.mainCamera.GetComponent<RaySensor>();
        var targets = cam.DetectedObjects;
        RaycastHit hit = new RaycastHit();
        foreach (var item in cam.DetectedObjects)
        {
            hit = cam.GetRayHit(item);
            objToAim.LookAt(hit.point);
            //headConstraintController.transform.position = hit.point;
            //rightArmIKController.transform.position = hit.point;
            if (item.layer == 12)
            {
                break;
            }
        }

    }

    public void SetIkWeight(GameObject _ik, float _weight)
    {
        _ik.GetComponentInParent<IRigConstraint>().weight = Mathf.Lerp(_ik.GetComponentInParent<IRigConstraint>().weight, _weight, .5f);
    }

    public void ChangePower()
    {
        currentPower.OnPowerExit(this);
        currentPower = powerList.Find(x => x.id == copyObject.copyPowerID);
        currentPower.OnPowerEnter(this);

        if (tutorialManager && !tutorialManager.SawPanel(4))
        {
            tutorialManager.HidePanel(3);
            tutorialManager.ShowPanel(4);
        }

    }

    private void ChangeVFXColor()
    {
        copyObject.VFXcolor = powerList.Find(x => x.id == copyObject.copyPowerID).powerColor;
        copyObject.axeCoreVFX.SetVector4("Color", copyObject.VFXcolor);
        copyObject.axeTrailVFX.SetVector4("Color", copyObject.VFXcolor);
    }




    public void ResetToCopyPower()
    {
        if (currentPower.id != 0)
        {
            copyObject.releasePowerVFX.PlayVFXColor(copyObject.VFXcolor);
            copyObject.axeCoreVFX.SetVector4("Color", copyObject.axeCoreBaseColor);
            copyObject.axeTrailVFX.SetVector4("Color", copyObject.axeCoreBaseColor);

            currentPower.OnPowerExit(this);
            currentPower = powerList.Find(x => x.id == 0);
            currentPower.OnPowerEnter(this);
            copyObject.copyPowerID = 0;
            FMODUnity.RuntimeManager.PlayOneShot("event:/Player/Axe_EnergyLost", transform.position);

            FillAmmo();

            onPowerReset?.Invoke();
        }

    }

    public void InputResetToCopyPower(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (tutorialManager)
            {
                tutorialManager.HidePanel(6);
            }
            ResetToCopyPower();
        }
    }
    public override void ParticleShoot()
    {
        AimToReticleTarget(weaponParticle.transform);
        weaponParticle.Play();
    }

    private void UpdateWeaponParticle()
    {

        weaponParticle = powerVFXList.Find(x => x.GetComponent<ParticleCollisionListener>().powerId == currentPower.id).GetComponent<ParticleSystem>();
        weaponParticle.GetComponent<ParticleCollisionListener>().hitMark = hitMark;
        weaponParticle.transform.position = shootPos.position; //p particula ficar um pouco na frente do player e n bater no collider


    }

    private void FillAmmo()
    {
        powerPoints.basePoints = currentPower.powerAmmo;
        powerPoints.FillPoints();

    }


    public void Shake()
    {
        hitPoints.Shake(1f, .5f, 2f);
    }

    public void PlayReturnToHandAnim()
    {
        controller.animator.SetTrigger("getAxe");
    }

    private void PlayJumpSFX()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/Player/Player_jump", transform.position);

    }

    private void PlayHitSFX()
    {
        playSFX.PlaySoundFX("event:/Player/Player_hit", 3f);
    }

    private void PlayDeathSFX()
    {
        playSFX.PlaySoundFX("event:/Player/Player_death", 0);
    }

    private void PlayFootStepSFX()
    {
        playSFX.PlaySoundFX("event:/Player/Player_footsteps", .06f);
    }

    public void Dash(InputAction.CallbackContext context)
    {
        if (context.performed && canDash)
        {
            if (tutorialManager && tutorialManager.SawPanel(5) && !tutorialManager.SawPanel(6))
            {
                tutorialManager.HidePanel(5);
                tutorialManager.ShowPanel(6);
                tutorialManager.StartCoroutine(tutorialManager.PanelSelfDisable(6, 7.5f));

            }


            if (controller.moveDirection == Vector3.zero)
                rb.AddForce(transform.forward * dashPower, ForceMode.Impulse);
            else
                rb.AddForce(controller.moveDirection * dashPower, ForceMode.Acceleration);

            StartCoroutine(DashCoolDown());
            FMODUnity.RuntimeManager.PlayOneShot("event:/Player/Player_dash", transform.position);

        }
    }

    IEnumerator DashCoolDown()
    {
        dashEnergyTrail.SetActive(true);
        canDash = false;
        yield return new WaitForSeconds(dashCoolDown);
        canDash = true;
        dashEnergyTrail.SetActive(false);

    }
    public IEnumerator Die()
    {
        controller.released = false;
        released = false;
        isDead = true;
        PlayDeathSFX();
        controller.animator.SetTrigger("dead");
        died = true;
        yield return new WaitForSeconds(6);

        GameState.Instance.PlayerDead();
        yield return null;
    }

}
