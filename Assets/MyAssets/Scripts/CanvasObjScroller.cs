﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasObjScroller : MonoBehaviour
{
    // Start is called before the first frame update
    private Slider slider;
    //public RectTransform objecToScroll;

    private void Start()
    {
        slider = GetComponent<Slider>();
    }

    public void MoveCanvasObj(RectTransform _gameObject)
    {
        _gameObject.anchoredPosition = new Vector2(_gameObject.anchoredPosition.x, slider.value);

    }
}
