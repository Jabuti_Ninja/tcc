﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    public LevelOptmizer levelOptmizer;
    public int areaId;


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerActor>())
            levelOptmizer.OnareaEnter(areaId);
    }
}
