﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelOptmizer : MonoBehaviour
{
    // Start is called before the first frame update

    public List<GameplayArea> areas = new List<GameplayArea>();
    public int currentAreaId;

    private void Start()
    {
        for (int i = 0; i < areas.Count; i++)
        {
            for (int j = 0; j < areas[i].gameObjects.Length; j++)
            {
                areas[i].gameObjects[j].SetActive(false);

            }
        }
    }

    public void OnareaEnter(int idArea)
    {
        for (int i = 0; i < areas[idArea].gameObjects.Length; i++)
        {
            areas[idArea].gameObjects[i].SetActive(true);
        }
    }

}
