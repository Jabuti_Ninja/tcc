﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySFX : MonoBehaviour
{
    private bool canPlay = true;
    public void PlaySoundFX(string path, float cooldown)
    {
        if (canPlay)
        {
            FMODUnity.RuntimeManager.PlayOneShot(path, transform.position);
            canPlay = false;
            StartCoroutine(AllowPlay(cooldown));
        }
    }


    IEnumerator AllowPlay(float cooldown)
    {
        yield return new WaitForSeconds(cooldown);
        canPlay = true;
    }
}
