﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Power_Copy", menuName = "Powers/ PowerCoy")]
public class PowerCopy : Power
{
    public float powerExecuteDelay = 0;

    public override void OnPowerEnter(PlayerActor actor)
    {
        base.OnPowerEnter(actor);
        
        if(actor.copyObject.state == CopyObject.State.InHand)
        {
            actor.copyObject.transform.parent = actor.copyObject.originalParent;
            actor.copyObject.ResetPosition();            
        }

    }

    public override void ExecutePrimaryPower(PlayerActor actor)
    {
        actor.StartCoroutine(actor.ExecuteCopy(powerExecuteDelay));
    }



}
