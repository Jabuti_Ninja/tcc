﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Powers/ Boitata")]
public class PowerBoitata : Power
{

    public float ammoDrainRate = .25f;
    public override void OnPowerExit(PlayerActor actor)
    {
        base.OnPowerExit(actor);
        var loopParticle = actor.weaponParticle.main;
        loopParticle.loop = false;
        actor.weaponParticle.Stop();
        actor.powerPoints.StopDrain();
        actor.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>().Stop();
        if (actor.fadeCoroutine != null)
            actor.StopCoroutine(actor.fadeCoroutine);


    }

    public override void ExecutePrimaryPower(PlayerActor actor)
    {
        
        actor.weaponParticle.Play();
        var loopParticle = actor.weaponParticle.main;
        loopParticle.loop = true;
        actor.powerPoints.StartDrain(primaryPowerAmmoConsume, ammoDrainRate);
        actor.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>().Play();


    }

    public override void ExecutePrimaryPowerRelease(PlayerActor actor)
    {
        base.ExecutePrimaryPowerRelease(actor);
        var loopParticle = actor.weaponParticle.main;
        loopParticle.loop = false;
        actor.powerPoints.StopDrain();
        actor.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>().Stop();

        if (actor.fadeCoroutine != null)
            actor.StopCoroutine(actor.fadeCoroutine);

    }

    //public override void ExecuteSecondaryPower(PlayerActor actor)
    //{
    //    base.ExecuteSecondaryPower(actor);
    //    actor.projectileShooting.ShootBullet(actor.projectileShotPos.position, actor.projectileShotPos.rotation);
    //}
}
