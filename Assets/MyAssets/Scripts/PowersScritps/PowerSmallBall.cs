﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(menuName = "Powers/ Curupira")]
public class PowerSmallBall : Power
{
    public float dashPower = 2;
    public float dashDuration = .75f;
    public float dashCooldown = 1f;

    private Coroutine currentCoroutine;

    public override void ExecutePrimaryPower(PlayerActor actor)
    {
        base.ExecutePrimaryPower(actor);
        actor.ParticleShoot();
        //actor.soundBank.PlaySoundBankAudio(actor.soundBank.primaryPower[actor.currentPower.id], GlobalAudioPlayer.Instance.soundType[(int)SoundType.PLAYERSHOTGUN], false);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Enemy/Enemy_shotgun", actor.transform.position);
    }

    public override void ExecuteSecondaryPower(PlayerActor actor)
    {        
        if(currentCoroutine == null)
        {
            //currentCoroutine = actor.StartCoroutine(actor.Dash(dashPower, dashDuration));
            //actor.StartCoroutine(ClearCoroutine(dashCooldown));
            //base.ExecuteSecondaryPower(actor);
        }        
    }

    private IEnumerator ClearCoroutine(float timer)
    {
        yield return new WaitForSeconds(timer);
        currentCoroutine = null;
        yield return null;
    }

}
