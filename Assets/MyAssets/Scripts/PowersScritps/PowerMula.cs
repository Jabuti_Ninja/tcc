﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Powers/ Mula")]
public class PowerMula : Power
{
    public override void ExecutePrimaryPower(PlayerActor actor)
    {
        base.ExecutePrimaryPower(actor);
        actor.projectileShooting.ShootBullet(actor.projectileShotPos.position, actor.projectileShotPos.rotation);
    }
}
