﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Power : ScriptableObject
{
    public string powerName;
    public int id;
    public int damage = 1;
    public int powerAmmo = 10;
    public int primaryPowerAmmoConsume = 0;
    public int secondaryPowerAmmoConsume = 0;
    public float fireRate = 0;
    public float secondaryFireRate = 0;

    [Header("Visual Settings")]
    public Color powerColor;
    public float colorIntensity;


    public virtual void OnPowerEnter(PlayerActor actor)
    {
        GameObject playerMesh = actor.GetComponentInChildren<SkinnedMeshRenderer>().gameObject;
        playerMesh.GetComponent<Renderer>().material.SetColor("_EmissionColor", powerColor);
        playerMesh.GetComponent<Renderer>().material.SetFloat("_EmissionIntensity", colorIntensity);
    }

    public virtual void OnPowerExit(PlayerActor actor)
    {

    }

    public virtual void ExecutePrimaryPower(PlayerActor actor)
    {
        actor.powerPoints.Damage(primaryPowerAmmoConsume);
    }

    public virtual void ExecutePrimaryPowerRelease(PlayerActor actor)
    {

    }


    public virtual void ExecuteSecondaryPower(PlayerActor actor)
    {
        actor.powerPoints.Damage(secondaryPowerAmmoConsume);
    }

    public virtual void ExecuteSecondaryPowerRelease(PlayerActor actor)
    {

    }
    
}
