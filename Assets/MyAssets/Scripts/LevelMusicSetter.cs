﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMusicSetter : MonoBehaviour
{

    public static int battleMusicCount;

    [SerializeField] private GameObject ambienceMusic;
    [SerializeField] private GameObject battleMusic;

    private void Update()
    {
        if (battleMusicCount > 0)
        {
            battleMusic.SetActive(true);
            ambienceMusic.SetActive(false);

        }
        else
        {
            battleMusic.SetActive(false);
            ambienceMusic.SetActive(true);
        }
    }

}
