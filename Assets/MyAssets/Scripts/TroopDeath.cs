﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TroopDeath : MonoBehaviour
{
    public delegate void OnDeath();
    public event OnDeath onDeath;

    private Animator animator;
    private StateController stateController;
    private AudioSource audioSource;
    private bool playedAudio;
    private SoundBank soundBank;

    private void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        stateController = GetComponent<StateController>();
        soundBank = stateController.soundBank;
    }

    private void OnEnable()
    {
        GetComponent<CapsuleCollider>().enabled = true;

    }

    public void CheckHP(HitPoints hitPoints)
    {
        if (hitPoints.currentPoints <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        animator.SetTrigger("dying");
        GetComponent<CapsuleCollider>().enabled = false;
        stateController.Dead();
        onDeath?.Invoke();
        transform.rotation = transform.rotation;
        //if (!audioSource.isPlaying && !playedAudio)
        //{
        //    audioSource.clip = (soundBank.death.audioClip[Random.Range(0, soundBank.death.audioClip.Length)]);
        //    audioSource.volume = soundBank.death.volume;
        //    audioSource.Play();
        //    playedAudio = true;
        //}
      //  if (!GetComponentInParent<EnemySpawner>())
        StartCoroutine("DisableEnemy");

    }

   // private void GetBackToSpawner()
   // {
   //     if (GetComponentInParent<EnemySpawner>())
   //     {
   //         GetComponentInParent<EnemySpawner>().EnemyDied(gameObject);
   //         stateController.StartCombat();
   //     }
   // }

    IEnumerator DisableEnemy()
    {
        yield return new WaitForSeconds(3f);
        gameObject.SetActive(false);
    }
}
