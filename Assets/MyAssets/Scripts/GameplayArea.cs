﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameplayArea
{
    public GameObject[] gameObjects = new GameObject[0];
}
