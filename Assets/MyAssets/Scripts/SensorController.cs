﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SensorToolkit;

public class SensorController: MonoBehaviour
{
    public RaySensor frontSensor;
    public RaySensor backSensor;
    public RaySensor rightSensor;
    public RaySensor leftSensor;
}
