﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;


public abstract class Points : MonoBehaviour
{

    public Slider slider;

    public int basePoints;
    /*[HideInInspector]*/
    public int currentPoints;

    public delegate void OnDamage();
    public virtual event OnDamage onDamage;

    public delegate void OnHeal();
    public event OnHeal onHeal;

    [HideInInspector]
    public Coroutine drainCoroutine;
    [HideInInspector]
    public Coroutine healOverTimeCoroutine;
    public int healOverTimeAmount;
    public float healOverTimeRate;

    public CinemachineVirtualCamera virtualCamera;
    private CinemachineBasicMultiChannelPerlin virtualCameraNoise;
    private bool startTimer;
    [HideInInspector] public float shakeElapsedTime;
    public virtual void Start()
    {
        if (virtualCamera)
            virtualCameraNoise = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        currentPoints = basePoints;
        if (slider != null)
        {
            slider.maxValue = basePoints;
        }
    }

    public virtual void Update()
    {
        CheckDeath();
        UpdateSlider();
        LimitPoints();
        if (virtualCamera)
        {
            if (startTimer)
            {
                shakeElapsedTime -= Time.deltaTime;
            }

            if (shakeElapsedTime <= 0)
            {
                StopShake();
            }
        }
    }

    public void UpdateSlider()
    {
        if (slider != null)
        {
            slider.value = currentPoints;
            slider.maxValue = basePoints;
        }
    }


    public void LimitPoints()
    {
        if (currentPoints > basePoints)
        {
            currentPoints = basePoints;
        }

        if (currentPoints < 0)
        {
            currentPoints = 0;
        }
    }

    private void OnEnable()
    {
        currentPoints = basePoints;
    }

    public virtual void Damage(int damageToDeal)
    {

        currentPoints -= damageToDeal;
        onDamage?.Invoke();

    }

    public void Heal(int healAmount)
    {
        currentPoints += healAmount;
        onHeal?.Invoke();
    }

    public void FillPoints()
    {
        currentPoints = basePoints;
    }

    private IEnumerator HealOverTime(int healAmount, float healRate)
    {
        while (true)
        {
            if (currentPoints < basePoints)
                Heal(healAmount);

            yield return new WaitForSeconds(healRate);
        }
    }

    public void StartHeal(int healAmount, float healRate)
    {
        healOverTimeCoroutine = StartCoroutine(HealOverTime(healAmount, healRate));
    }

    public void StopHeal()
    {
        if (healOverTimeCoroutine != null)
        {
            StopCoroutine(healOverTimeCoroutine);
            healOverTimeCoroutine = null;
        }

    }

    private IEnumerator Drain(int drainAmount, float drainRate)
    {
        while (true)
        {
            Damage(drainAmount);
            yield return new WaitForSeconds(drainRate);
        }
    }

    public void StartDrain(int drainAmount, float drainRate)
    {
        drainCoroutine = StartCoroutine(Drain(drainAmount, drainRate));
    }

    public void StopDrain()
    {
        if (drainCoroutine != null)
            StopCoroutine(drainCoroutine);
    }

    public void Shake(float _shakeDuration, float _shakeAmplitude, float _shakeFrequency)
    {
        if (virtualCamera && virtualCameraNoise)
        {
            startTimer = true;
            shakeElapsedTime = _shakeDuration;
            if (startTimer)
            {
                virtualCameraNoise.m_AmplitudeGain = _shakeAmplitude;
                virtualCameraNoise.m_FrequencyGain = _shakeFrequency;
            }

        }
    }

    public void StopShake()
    {
        startTimer = false;
        virtualCameraNoise.m_AmplitudeGain = 0;
        shakeElapsedTime = 0;
    }

    public void CheckDeath()
    {
        if(currentPoints == 0)
        {
            if (GetComponent<Rigidbody>())
                GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

}
