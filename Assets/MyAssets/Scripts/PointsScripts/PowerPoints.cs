﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerPoints : Points
{
    [HideInInspector] public PlayerActor playerActor;

    public override event OnDamage onDamage;
    public Image powerBarFill;
    public GameObject powerBar;

    public override void Start()
    {
        playerActor = GetComponent<PlayerActor>();
    }


    // Update is called once per frame
    public override void Update()
    {
        //UpdateSlider();
        //LimitPoints();
        UpdatePowerBar();
        //LimitImageFill();
        if (playerActor.currentPower.id == 0)
        {
            powerBar.gameObject.SetActive(false);
        }
        else
            powerBar.gameObject.SetActive(true);
    }


    public void PowerBarDamage(int damageToDeal)
    {
        currentPoints -= damageToDeal;
        onDamage?.Invoke();
    }

    public void UpdatePowerBar()
    {
        powerBarFill.fillAmount = (float) 0.08f + (currentPoints/(basePoints/(0.42f-0.08f)));

    }

    //public void LimitImageFill()
    //{
    //    if (currentPoints / (basePoints / (0.42f)) > 0.42)
    //    {
    //        currentPoints = basePoints;
    //    }

    //    if (currentPoints / (basePoints / (0.5f)) < 0.08)
    //    {
    //        currentPoints = 0;
    //    }
    //}

}
