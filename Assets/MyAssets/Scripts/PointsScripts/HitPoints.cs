﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


public class HitPoints : Points
{
    public bool invulnerable = false;
    private bool runLastTimeHittedTimer;
    [HideInInspector] public float lastTimeHitted;
    [SerializeField] private float autoHealDelay;

    // public float timeBetweenHitAllow = 0;
    public GameObject[] lowLifeFeedback;
    //public override event OnDamage onDamage;
    private bool isPlayer;

    [SerializeField] private TutorialManager tutorialManager;

    public override void Start()
    {
        base.Start();
        isPlayer = GetComponent<PlayerActor>();
    }

    public override void Update()
    {
        base.Update();
        if (runLastTimeHittedTimer)
        {
            lastTimeHitted += Time.deltaTime;
        }

        if (currentPoints <= 0)
        {
            HideSlider();
        }


        if (isPlayer)
        {
            if (lastTimeHitted > autoHealDelay)
            {
                if (healOverTimeCoroutine == null)
                {
                    StartHeal(healOverTimeAmount, healOverTimeRate);
                    if (tutorialManager && !tutorialManager.SawPanel(7) && TutorialManager.lastPanelSeen == 6)
                    {
                        tutorialManager.HidePanel(6);
                        tutorialManager.ShowPanel(7);
                        tutorialManager.StartCoroutine(tutorialManager.PanelSelfDisable(7, 7.5f));
                    }
                }
            }

            if (currentPoints <= ((float)basePoints / 100) * 30 && currentPoints > 0)
            {
                foreach (var item in lowLifeFeedback)
                {
                    item.SetActive(true);
                }

            }
            else
                foreach (var item in lowLifeFeedback)
                {
                    item.SetActive(false);
                }
        }
        if (!isPlayer && slider.tag != "BossSlider")
        {
            slider.GetComponent<RectTransform>().LookAt(GetComponent<StateController>().target);

            if (currentPoints < ((float)basePoints / 100) * 50 && currentPoints > 0)
            {
                ShowSlider();
            }
            else if (currentPoints > ((float)basePoints / 100) * 50 && lastTimeHitted > 2f)
            {
                HideSlider();
            }

        }
    }

    public override void Damage(int damageToDeal)
    {
        if (!invulnerable/* && lastTimeHitted > timeBetweenHitAllow*/)
        {
            if (!isPlayer && slider.tag != "BossSlider")
            {
                ShowSlider();
            }
            StartTimer();
            base.Damage(damageToDeal);
            //currentPoints -= damageToDeal;
            //onDamage?.Invoke();

        }
    }

    public void StartTimer()
    {
        runLastTimeHittedTimer = true;
        lastTimeHitted = 0;
        StopHeal();

    }

    public void ShowSlider()
    {
        slider.gameObject.SetActive(true);
    }

    public void HideSlider()
    {
        slider.gameObject.SetActive(false);
    }
}
