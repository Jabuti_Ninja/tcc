﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SoundBank_", menuName = "SoundBank")]
public class SoundBank : ScriptableObject
{
    public SoundData step;
    [Range(-3f, 3f)]
    public float stepPitchMin;
    [Range(-3f, 3f)]
    public float stepPitchMax;

    public SoundData takeHit;

    public SoundData dash;
    public SoundData crouch;
    public SoundData jump;
    public SoundData death;

    [Header("Enemy Only")]
    public SoundData primaryAttack;
    public SoundData secondaryAttack;

    [Header("Player Only")]
    public SoundData[] primaryPower;
    public SoundData[] secondaryPower;

    public SoundData axeMoving;
    public SoundData axeHit;
    public SoundData axeThrow;
    public SoundData axeCatch;

    public void PlaySoundBankAudio(SoundData soundData, AudioSource audioSource, bool isLoop)
    {
        if(soundData.audioClip.Length > 0)
        {
            audioSource.volume = soundData.volume;
            audioSource.loop = isLoop;
            if(isLoop)
            {
                audioSource.clip = soundData.audioClip[Random.Range(0, soundData.audioClip.Length)];
                audioSource.Play();
            }
            else
                audioSource.PlayOneShot(soundData.audioClip[Random.Range(0, soundData.audioClip.Length)], soundData.volume);
        }

    }




}
