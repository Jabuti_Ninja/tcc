﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorVFXController : MonoBehaviour
{
    private Animator animator;
    public string animParameter;
    private ParticleSystem pSystem;
    
    void Start()
    {
        animator = GetComponent<Animator>();
        pSystem = GetComponent<ParticleSystem>();
    }

    void Update()
    {
        animator.SetBool(animParameter, pSystem.isPlaying);
    }
}
