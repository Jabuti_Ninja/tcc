﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class HackPower : MonoBehaviour
{
    private Collider col;
    private Coroutine coroutine;
    private void Start()
    {
        col = GetComponent<Collider>();
    }
    public void HackPowerAttack(InputAction.CallbackContext context)
    {
        if (context.performed && coroutine == null)
        {
            col.enabled = true;
            coroutine = StartCoroutine(SelfDisable());
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        StateController enemy = other.GetComponentInParent<StateController>();

        if (enemy)
            enemy.hitPoints.currentPoints = 0; 

    }

    IEnumerator SelfDisable()
    {
        yield return new WaitForSeconds(1F);
        col.enabled = false;
        coroutine = null;
    }
}
