﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundType
{
    HIT,
    PORTALEXPLOSION,
    PLAYERLOOP,
    PLAYERSHOTGUN,
    ENEMYSHOTGUN
}

public class GlobalAudioPlayer : MonoBehaviour
{
    public static GlobalAudioPlayer Instance { get; private set; }
    [HideInInspector] public AudioSource audioSource;
    public AudioSource[] soundType;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        Instance = this;
    }

    public void PlaySound(AudioClip audioClip, SoundType type, float volume)
    {
        soundType[(int)type].volume = volume;
        soundType[(int)type].PlayOneShot(audioClip, volume);
    }
}
