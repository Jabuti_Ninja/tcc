﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleBarrier : MonoBehaviour
{
    [SerializeField] public ParticleSystem hitVFX;
    public bool onlyWhenOnLayer;


    private void OnCollisionStay(Collision collision)
    {
        var axe = collision.gameObject.GetComponent<CopyObject>();


        if (axe && axe.state != CopyObject.State.InHand)
        {
            if (onlyWhenOnLayer)
            {
                if (gameObject.layer != 11)
                    return;
            }

            FMODUnity.RuntimeManager.PlayOneShot("event:/Puzzle/Portal_explosion", transform.position);
            axe.state = CopyObject.State.Returning;
            if(hitVFX)
            {
                hitVFX.transform.position = collision.transform.position;
                hitVFX.Play();
            }
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        var axe = collision.gameObject.GetComponent<CopyObject>();


        if (axe && axe.state != CopyObject.State.InHand)
        {
            if (onlyWhenOnLayer)
            {
                if (gameObject.layer != 11)
                    return;
            }

            FMODUnity.RuntimeManager.PlayOneShot("event:/Puzzle/Portal_explosion", transform.position);
            axe.state = CopyObject.State.Returning;
            if (hitVFX)
            {
                hitVFX.transform.position = collision.transform.position;
                hitVFX.Play();
            }
        }
    }
}
