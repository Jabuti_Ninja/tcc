﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleObject : MonoBehaviour
{
    public bool selectAction;
    public float timeToDeselect;
    private bool selected;
    private float timer;
    private bool startTimer;

    public PuzzleObjectAction objectAction;
    public GameObject puzzleActor;

    [SerializeField] private GameObject explosionParticle;
    [SerializeField] private GameObject idleParticle;

    private Animator animator;
    private FMODUnity.StudioEventEmitter fmodPlayer;

    private CopyObject axe;
    private bool gotBack;
    private void Start()
    {
        objectAction.playingTicTac = false;

        animator = GetComponent<Animator>();
        fmodPlayer = GetComponent<FMODUnity.StudioEventEmitter>();

        if (GetComponentInParent<PuzzleDoor>()) puzzleActor = transform.parent.gameObject;
    }

    private void Update()
    {
        if (selectAction)
        {
            if (startTimer)
            {
                timer += Time.deltaTime;
                if (timer > timeToDeselect)
                {
                    objectAction.Desact(gameObject, puzzleActor);
                    animator.SetBool("isBlue", false);
                    fmodPlayer.Stop();
                    startTimer = false;
                    timer = 0;
                    selected = false;
                    objectAction.playingTicTac = false;

                }
            }

            if (objectAction.CheckListCount(puzzleActor) == 0)
            {
                if (axe && !gotBack)
                {
                    axe.state = CopyObject.State.Returning;
                    gotBack = true;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        axe = other.GetComponent<CopyObject>();
        if (axe)
        {
            if (!selectAction)
            {
                objectAction.Act(gameObject, puzzleActor);
                explosionParticle.SetActive(true);
                idleParticle.SetActive(false);
                FMODUnity.RuntimeManager.PlayOneShot("event:/Puzzle/Portal_explosion", transform.position);
                axe.state = CopyObject.State.Returning;
                if (gameObject.activeInHierarchy)
                    StartCoroutine(SelfDisable());
            }

            if (selectAction)
            {
                objectAction.Act(gameObject, puzzleActor);
                FMODUnity.RuntimeManager.PlayOneShot("event:/Puzzle/Portal_explosion", transform.position);
                StartCoroutine(SetSelect());
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        axe = other.GetComponent<CopyObject>();
        if (axe)
        {
            if (selectAction)
            {
                objectAction.Act(gameObject, puzzleActor);
                StartCoroutine(SetSelect());

            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        axe = other.GetComponent<CopyObject>();
        if (axe)
        {
            if (selectAction && selected)
            {
                startTimer = true;
            }
        }
    }

    IEnumerator SetSelect()
    {
        yield return new WaitForSeconds(.01f);
        selected = true;
        if (!animator.GetBool("isBlue"))
        {
            animator.SetBool("isBlue", true);
        }

        if (!fmodPlayer.IsPlaying() && !objectAction.playingTicTac)
        {
            fmodPlayer.Play();
            objectAction.playingTicTac = true;
        }
    }

    IEnumerator SelfDisable()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
