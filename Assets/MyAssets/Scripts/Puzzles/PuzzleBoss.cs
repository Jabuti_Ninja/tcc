﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleBoss : Puzzle
{
    public State stateToChange;
    public StateController stateController;

    public override void Act()
    {

        
        if (stateController.currentState != stateToChange)
        {
            stateController.ChangeState(stateToChange);
        }
        gameObject.SetActive(false);

    }
}
