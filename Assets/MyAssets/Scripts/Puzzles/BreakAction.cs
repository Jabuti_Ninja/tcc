﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PuzzleObjectAction_Break", menuName = "Puzzle/PuzzleObjectAction_Break")]
public class BreakAction : PuzzleObjectAction
{
    public override void Act(GameObject _gameObject, GameObject _objectToact)
    {
        RemoveFromList(_gameObject, _objectToact);
        CheckOpen(_objectToact);
    }
}
