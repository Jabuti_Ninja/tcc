﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPuzzles : MonoBehaviour
{
    public Puzzle[] puzzles;
    public GameObject collidersParent;
    [HideInInspector] public int currentBossPhase = 1;

}
