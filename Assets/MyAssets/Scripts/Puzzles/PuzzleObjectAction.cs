﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleObjectAction : ScriptableObject
{
    // Start is called before the first frame update
    [HideInInspector] public bool playingTicTac;

    public virtual void Act(GameObject gameObject, GameObject _objectToact)
    {

    }



    public virtual void Desact(GameObject gameObject, GameObject _objectToact)
    {

    }

    public virtual void RemoveFromList(GameObject gameObject, GameObject _objectToact)
    {
        _objectToact.GetComponent<Puzzle>().puzzleObjects.Remove(gameObject);
    }

    public virtual void ReturnToList(GameObject gameObject, GameObject _objectToact)
    {
        _objectToact.GetComponent<Puzzle>().puzzleObjects.Add(gameObject);
    }


    public virtual void CheckOpen(GameObject gameObject)
    {
        if (CheckListCount(gameObject) == 0)
        {
            gameObject.GetComponent<Puzzle>().Act();
        }
    }

    public virtual int CheckListCount(GameObject gameObject)
    {
        return gameObject.GetComponent<Puzzle>().puzzleObjects.Count;
    }

    public virtual void SetObjectActive(GameObject _gameObject,  bool _active)
    {
        if(_gameObject.activeSelf != _active)
        _gameObject.SetActive(_active);
    }
}
