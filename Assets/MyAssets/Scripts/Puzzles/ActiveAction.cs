﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PuzzleObjectAction_Active", menuName = "Puzzle/PuzzleObjectAction_Active")]

public class ActiveAction : PuzzleObjectAction
{
    public override void Act(GameObject _gameObject, GameObject _objectToAct)
    {
         _objectToAct.SetActive(true);
    }

    public override void Desact(GameObject _gameObject, GameObject _objectToAct)
    {
        _objectToAct.SetActive(false);
    }

}
