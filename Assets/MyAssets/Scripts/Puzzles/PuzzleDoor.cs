﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleDoor : Puzzle
{
    // Start is called before the first frame update

    //public ParticleSystem fireBarrier;
    //public ParticleSystem fireEmbers;
    public GameObject energyWall;
    public GameObject barrier;
    public BoxCollider colliderBarrier;
    //private ParticleSystem.EmissionModule fireBarrierEmission;
    //private ParticleSystem.EmissionModule fireEmbersEmission;
    [HideInInspector] public bool playingTicTac;


    private void Start()
    {
        //fireBarrierEmission = fireBarrier.emission;
        //fireEmbersEmission = fireEmbers.emission;
    }

    private void Update()
    {
        if (barrier)
        {
            if (GetComponentInChildren<PuzzleObject>().selectAction)
            {
                if (puzzleObjects.Count == 2)
                {
                    barrier.GetComponent<Animator>().SetBool("isBlue", false);
                }
                else if (puzzleObjects.Count == 1)
                {
                    barrier.GetComponent<Animator>().SetBool("isBlue", true);
                }
            }
        }
    }

    public override void Act()
    {
        Open();
    }


    public void Open()
    {

        //fireBarrierEmission.enabled = false;
        //fireEmbersEmission.enabled = false;
        energyWall.GetComponent<Animator>().SetTrigger("destroy1");
        colliderBarrier.enabled = false;
        FMODUnity.RuntimeManager.PlayOneShot("event:/Puzzle/Portal_opening", transform.position);

        StartCoroutine(Desactive());
    }


    IEnumerator Desactive()
    {
        yield return new WaitForSeconds(5f);
        gameObject.SetActive(false);
    }
}
