﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Puzzle : MonoBehaviour
{
    public List<GameObject> puzzleObjects = new List<GameObject>();


    public abstract void Act();
}
