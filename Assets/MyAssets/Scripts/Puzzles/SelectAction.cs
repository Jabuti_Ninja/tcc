﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[CreateAssetMenu(fileName = "PuzzleObjectAction_Select", menuName = "Puzzle/PuzzleObjectAction_Select")]

public class SelectAction : PuzzleObjectAction
{

    public override void Act(GameObject _gameObject, GameObject _objectToAct)
    {
        RemoveFromList(_gameObject, _objectToAct);
        CheckOpen(_objectToAct);
    }

    public override void Desact(GameObject gameObject, GameObject _objectToAct)
    {
        ReturnToList(gameObject, _objectToAct);

    }

}
