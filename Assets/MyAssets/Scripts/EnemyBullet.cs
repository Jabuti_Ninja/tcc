﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{

    [HideInInspector] public int damage = 1;
    [SerializeField] float baseLifeTime = 5f;
    [SerializeField] private bool destroyOnCollision = true;
    [SerializeField] private string target = "Player";

    public bool autoResetParent = false;
    public Transform originalParent;

    private List<Actor> enemiesDamaged = new List<Actor>();





    private void Start()
    {
        OnEnable();
    }

    private void OnEnable()
    {
        StartCoroutine(AutoSelfDestruct());
    }

    private void OnDisable()
    {
        enemiesDamaged.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(target))
        {
            var enemy = other.GetComponent<Actor>();
            if (!enemy)
                enemy = other.GetComponentInParent<Actor>();

            if (!enemiesDamaged.Contains(enemy))
            {
                //var hitPoints = other.GetComponent<HitPoints>();
                //if (!hitPoints)
                //    hitPoints = other.GetComponentInParent<HitPoints>();

                //if (hitPoints)
                //{
                //    hitPoints.Damage(damage);
                //}
                if (enemy)
                {
                    enemy.TakeDamage(damage);
                    enemiesDamaged.Add(enemy);
                }
            }

        }
        if (destroyOnCollision)
            SelfDestruct();


    }

    private IEnumerator AutoSelfDestruct()
    {
        yield return new WaitForSeconds(baseLifeTime);
        SelfDestruct();
        yield return null;
    }

    private void SelfDestruct()
    {
        if (autoResetParent)
        {
            transform.parent = originalParent;
        }
        if (originalParent)
            ObjectPooler.SharedInstance.PoolDestroy(originalParent.gameObject);
        ObjectPooler.SharedInstance.PoolDestroy(gameObject);
    }
}
