﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualPowerColorUpdate : MonoBehaviour
{
    private Image image;

    private void Start()
    {
        image = GetComponent<Image>();
    }

    private void OnEnable()
    {
        StartCoroutine(EnableCoroutine());
    }
    private void OnDisable()
    {
        PlayerActor.playerInstance.copyObject.onPowerChange -= ChangeColor;
        PlayerActor.playerInstance.onPowerReset -= ChangeColor;
    }


    private IEnumerator EnableCoroutine()
    {
        yield return new WaitForEndOfFrame();
        PlayerActor.playerInstance.copyObject.onPowerChange += ChangeColor;
        PlayerActor.playerInstance.onPowerReset += ChangeColor;
        yield return null;
    }


    private void ChangeColor()
    {
        if (PlayerActor.playerInstance.currentPower.id != 0)
            image.color = PlayerActor.playerInstance.currentPower.powerColor;
        else image.color = Color.black;
    }
}
