﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using DG.Tweening;
using System;

public class CopyObject : MonoBehaviour
{
    public int copyPowerID;
    [HideInInspector] public Collider col;
    private Rigidbody rb;

    [HideInInspector] public SoundBank soundBank;


    [Header("Axe Settings")]
    [SerializeField] private float autoReturnDelay = 4;
    private Coroutine autoReturnCoroutine;

    [HideInInspector] public Transform originalParent;

    public float throwForce = 10;
    public float returnSpeed = 15;
    public float throwTurnSpeed = 50;
    public float returnSpdIncreaseRate = .01f;
    private float returnSpeedMultiplier = 1;
    public float maxReturnMultiplier = 3;


    public enum State { InHand, Stopped, Following, Returning, GoingForward };
    public State state = State.InHand;
    public bool inHand = true;

    public Transform throwPos;
    public Transform sheathPos;

    public delegate void OnPowerChange();
    public event OnPowerChange onPowerChange;

    public delegate void OnEnemyHit();
    public event OnEnemyHit onEnemyHit;

    public delegate void OnHandReturn();
    public event OnHandReturn onHandReturn;
    [HideInInspector] public GameObject hitMark;
    //tutorial
    [HideInInspector] public TutorialManager tutorialManager;

    //VISUAL
    [Header("Visual Settings")]

    public Transform axeModel;
    public float axeRotationSpeed = 10;
    [SerializeField]
    public VisualEffect axeTrailVFX;
    private float baseAxeTrailSpawn;

    [SerializeField]
    public VisualEffect axeCoreVFX;
    [HideInInspector]
    public Color axeCoreBaseColor;

    public ParticleColor absorbPowerVFX;
    public ParticleColor gainPowerVFX;
    public ParticleColor releasePowerVFX;
    public Color VFXcolor;


    private FMODUnity.StudioEventEmitter fmod;
    private void Start()
    {
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = true;
        originalParent = transform.parent;
        axeCoreBaseColor = axeCoreVFX.GetVector4("Color");
        baseAxeTrailSpawn = axeTrailVFX.GetFloat("Spawn");

        StopAxeTrail();

        fmod = GetComponent<FMODUnity.StudioEventEmitter>();
    }


    private void Update()
    {
        //Debug.Log("Estado do machado: " + state);
        switch (state)
        {
            // case State.Following:
            //     FollowTarget(targetToFollow, throwForce, throwTurnSpeed);
            //     break;

            case State.InHand:
                ReturnToHand();
                break;

            case State.Returning:
                FollowTarget(originalParent, returnSpeed, throwTurnSpeed);
                break;

            case State.Stopped:
                StopMoving();
                break;

            case State.GoingForward:
                GoFoward();
                break;
        }

        if (state == State.InHand)
        {
            if (transform.parent != originalParent && transform.parent != sheathPos)
                Debug.Log("ta errado");

        }
    }


    public void Throw(State stateToGo, float throwDelay)
    {
        StartCoroutine(ThrowCoroutine(stateToGo, throwDelay));
    }

    public IEnumerator ThrowCoroutine(State stateToGo, float delay)
    {
        yield return new WaitForSeconds(delay);
        transform.parent = null;
        transform.position = throwPos.position;
        transform.rotation = throwPos.rotation;
        state = stateToGo;
        FMODUnity.RuntimeManager.PlayOneShot("event:/Player/Player_axeThrow", transform.position);
    }


    private void FollowTarget(Transform target, float force, float rotationForce)
    {
        rb.isKinematic = false;
        Vector3 direction = target.position - rb.position;
        direction.Normalize();
        Vector3 rotationAmount = Vector3.Cross(transform.forward, direction);
        rb.angularVelocity = rotationAmount * rotationForce;

        rb.velocity = direction * force * returnSpeedMultiplier;
        if (returnSpeedMultiplier <= maxReturnMultiplier)
            returnSpeedMultiplier *= 1 + returnSpdIncreaseRate;

        SpinAxe();

        StopAutoReturn();
        StartAxeTrail();
    }


    private void GoFoward()
    {
        transform.parent = null;
        inHand = false;
        rb.isKinematic = false;
        rb.velocity = transform.forward * throwForce;
        rb.angularVelocity = new Vector3(0, 0, 0);
        SpinAxe();

        if (tutorialManager && !tutorialManager.SawPanel(2))
        {
            tutorialManager.ShowPanel(2);
            tutorialManager.HidePanel(1);
            tutorialManager.StartCoroutine(tutorialManager.PanelSelfDisable(2, 5f));
        }

        if (autoReturnCoroutine == null)
        {
            autoReturnCoroutine = StartCoroutine(AutoReturn(autoReturnDelay));
        }

        StartAxeTrail();

    }

    public void ReturnToHand()
    {
        if (!inHand)
        {
            if (tutorialManager && !tutorialManager.SawPanel(3))
            {
                if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 4)
                {
                    tutorialManager.ShowPanel(3);
                    tutorialManager.HidePanel(2);
                }
            }
            if (copyPowerID == 0)
            {
                transform.parent = originalParent;
            }
            else transform.parent = sheathPos;

            ResetPosition();
            FMODUnity.RuntimeManager.PlayOneShot("event:/Player/Player_axeCatch", transform.position);

            if (copyPowerID != 0)
            {
                onPowerChange?.Invoke();

                gainPowerVFX.PlayVFXColor(VFXcolor);
                FMODUnity.RuntimeManager.PlayOneShot("event:/Player/Axe_EnergyObtained", transform.position);
            }
            onHandReturn?.Invoke();
            inHand = true;
        }
        
    }

    public void ResetPosition()
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
        axeModel.transform.localPosition = Vector3.zero;
        axeModel.localRotation = Quaternion.Euler(0, 0, 0);
        StopMoving();
    }

    private void StopMoving()
    {
        rb.isKinematic = true;
        rb.angularVelocity = new Vector3(0, 0, 0);
        rb.velocity = Vector3.zero;
        returnSpeedMultiplier = 1;

        StopAutoReturn();
        StopAxeTrail();
        fmod.Stop();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != 11)
        {
            //Pega o poder
            if (state != State.InHand)
            {
                var enemyActor = other.GetComponentInParent<StateController>();

                //  if(enemyActor)
                //      Debug.Log(" ENEMY ACTOR FOUND: " + enemyActor.name);

                var puzzleBarrier = other.GetComponent<PuzzleBarrier>();
                if (!enemyActor)
                {
                    enemyActor = other.GetComponent<StateController>();
                    // Debug.Log(" ENEMY ACTOR FOUND: " + enemyActor);
                }

                if (enemyActor && !enemyActor.dead && copyPowerID == 0)
                {
                    //Debug.Log("FOUND POWER TO COPY");
                    if (enemyActor.hitPoints.invulnerable)
                    {
                        //   Debug.Log("ENEMY IS INVULNERABLE");
                        return;
                    }



                    copyPowerID = enemyActor.troopData.powerId;
                    // Debug.Log("POWER COPIED: " + copyPowerID);
                    onEnemyHit?.Invoke();
                    transform.parent = enemyActor.transform;
                    enemyActor.Hitstun();

                    absorbPowerVFX.PlayVFXColor(VFXcolor);




                    if (hitMark)
                        hitMark.SetActive(true);
                }
                else if (puzzleBarrier)
                {
                    if (puzzleBarrier.hitVFX)
                    {
                        puzzleBarrier.hitVFX.transform.position = transform.position;
                        puzzleBarrier.hitVFX.Play();
                    }

                    state = State.Returning;
                }

            }

            if (state == State.Following || state == State.GoingForward) //p não bater na mão
            {
                if (!other.CompareTag("Player"))
                {
                    state = State.Stopped;
                    FMODUnity.RuntimeManager.PlayOneShot("event:/Player/Player_axeHit", transform.position);

                }
            }
        }

    }


    private void OnTriggerStay(Collider other)
    {
        PlayerActor playerActor = other.gameObject.GetComponentInParent<PlayerActor>();
        if (playerActor && state == State.Returning)
        {
            state = State.InHand;
        }
    }

    public void SpinAxe()
    {
        axeModel.Rotate(axeRotationSpeed * Time.deltaTime, 0, 0);
        if (!fmod.IsPlaying())
            fmod.Play();
    }

    private IEnumerator AutoReturn(float returnDelay)
    {
        yield return new WaitForSeconds(returnDelay);

        state = State.Returning;

        yield return null;
    }

    private void StopAutoReturn()
    {
        if (autoReturnCoroutine != null)
        {
            StopCoroutine(autoReturnCoroutine);
            autoReturnCoroutine = null;
        }
    }

    //VIUSAL
    private void StartAxeTrail()
    {
        axeTrailVFX.SetFloat("Spawn", baseAxeTrailSpawn);
    }

    private void StopAxeTrail()
    {
        axeTrailVFX.SetFloat("Spawn", 0);
    }

}
