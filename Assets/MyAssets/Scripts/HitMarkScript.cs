﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitMarkScript : MonoBehaviour
{
    public float timeToDisappear;

    private void OnEnable()
    {
        StartCoroutine(Disappear());
    }

    IEnumerator Disappear()
    {
        yield return new WaitForSeconds(timeToDisappear);
        gameObject.SetActive(false);
    }
}
