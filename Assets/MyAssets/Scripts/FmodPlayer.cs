﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FmodPlayer : MonoBehaviour
{
    public void PlayFootstepEvent(string path)
    {
        FMODUnity.RuntimeManager.PlayOneShot(path, transform.position);
    }

}
