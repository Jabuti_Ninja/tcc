﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MulaPowerProjectile : MonoBehaviour
{
    [SerializeField] private EnemyBullet explosion;
    [SerializeField] private GameObject disableObjects;
    

    [Header("General Settings")]
    public float baseLifeTime = 4f;
    public int damage = 20;

    private void OnEnable()
    {
        // StartCoroutine(AutoSelfDestruct());
        explosion.gameObject.SetActive(false);
        disableObjects.SetActive(true);
    }
    public void OnTriggerEnter(Collider other)
    {        
        //if (other.tag != "Player")
        //{
        //    Debug.Log(other.name);
        //    SelfDestruct();
        //}
            
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            SelfDestruct();
        }
    }

    private IEnumerator AutoSelfDestruct()
    {
        yield return new WaitForSeconds(baseLifeTime);
        SelfDestruct();
        yield return null;
    }

    private void SelfDestruct()
    {
        StopAllCoroutines();
        explosion.gameObject.SetActive(true);
        explosion.damage = damage;
        explosion.originalParent = transform;
        explosion.transform.position = transform.position;
        explosion.transform.parent = null;
        disableObjects.SetActive(false);
        //ObjectPooler.SharedInstance.PoolDestroy(gameObject);
    }
}
