﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SoundData
{
    public AudioClip[] audioClip = new AudioClip[0];
    public float volume = 1;
}
