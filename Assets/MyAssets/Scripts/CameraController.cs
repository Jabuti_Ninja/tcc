﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraController : MonoBehaviour
{

    private PlayerActor playerActor;
    public float sensitivityX;
    public float sensitivityY;
    public float maxYAngle = 80f;
    private Vector2 currentRotation;

    [HideInInspector] public bool canRotate = true;

    void Start()
    {
        playerActor = GetComponentInParent<PlayerActor>();
        sensitivityX = PlayerPrefs.GetFloat("sensitivityX");
        sensitivityY = PlayerPrefs.GetFloat("sensitivityY");
    }

    private void Update()
    {
        if (canRotate)
            UpdateCameraRotation();
    }

    private void UpdateCameraRotation()
    {
        currentRotation.x += playerActor.controller.inputActions.Player.Look.ReadValue<Vector2>().x * sensitivityX * Time.deltaTime;
        currentRotation.y -= playerActor.controller.inputActions.Player.Look.ReadValue<Vector2>().y * sensitivityY * Time.deltaTime;
        currentRotation.x = Mathf.Repeat(currentRotation.x, 360);
        currentRotation.y = Mathf.Clamp(currentRotation.y, -maxYAngle, maxYAngle);
        transform.rotation = Quaternion.Euler(currentRotation.y, 180 + currentRotation.x, 0);

    }
}
