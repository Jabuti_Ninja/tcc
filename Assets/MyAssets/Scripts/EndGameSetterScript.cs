﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameSetterScript : MonoBehaviour
{
    public static bool passedLevel0;

    [SerializeField] private GameObject[] objectsToDesactive;
    [SerializeField] private GameObject player;

    [SerializeField] private GameObject copyObject;
    [SerializeField] private GameObject cutsceneAxe;

    private bool playedFinalCutscene;
    private bool isLevel0;

    private void Start()
    {
        isLevel0 = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 3;
        if (passedLevel0 && isLevel0)
        {
            DesactiveObjects();
            player.transform.position = transform.position;
            player.transform.rotation = transform.rotation;
        }
    }
    private void Update()
    {
        if (isLevel0 && CutscenesManager.Instance.currentCutscene.type == CutsceneType.CS16 && !playedFinalCutscene)
        {
            StartCoroutine(FinalCutsceneAxesSetter());
            playedFinalCutscene = true;
        }
    }



    private void DesactiveObjects()
    {
        for (int i = 0; i < objectsToDesactive.Length; i++)
        {
            objectsToDesactive[i].SetActive(false);
        }
    }

    public IEnumerator FinalCutsceneAxesSetter()
    {
        yield return new WaitForSeconds(1f);
        copyObject.SetActive(false);
        cutsceneAxe.SetActive(true);
    }

    public void ResetLevel0()
    {
        passedLevel0 = false;
    }
}
