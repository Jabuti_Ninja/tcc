﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public GameObject[] panels;
    public static int lastPanelSeen;

    private int index;

    private void Start()
    {
        //if (PlayerPrefs.GetInt("tutorial", 0) == 0)
        //{
        //    gameObject.SetActive(false);
        //}else
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 3)
            ShowPanel(0);
    }

    public void ShowPanel(int panelId)
    {
        panels[panelId].SetActive(true);
        lastPanelSeen = panelId;
        index++;
    }

    public void HidePanel(int panelId)
    {
        //panels[panelId].SetActive(false);
        panels[panelId].GetComponent<Animator>().SetBool("On", true);

        if (index == panels.Length)
        {
            StartCoroutine(Deactivate());
        }

    }

    public IEnumerator Deactivate()
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(false);
        }
    }

    public IEnumerator PanelSelfDisable(int _panelID, float timeToDesactive)
    {
        yield return new WaitForSeconds(timeToDesactive);
        panels[_panelID].SetActive(false);

    }

    public bool SawPanel(int panelId)
    {
        if (panelId > lastPanelSeen)
            return false;
        else return true;
    }
}
