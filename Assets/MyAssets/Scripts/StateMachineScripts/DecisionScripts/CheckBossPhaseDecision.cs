﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Decision_", menuName = "Troop/Decisions/CheckBossPhase")]
public class CheckBossPhaseDecision : Decision
{
    [Range(1,3)]public int phaseToCheck;
    public override bool Decide(StateController controller)
    {
        return phaseToCheck == controller.GetComponent<BossPuzzles>().currentBossPhase;

    }
}
