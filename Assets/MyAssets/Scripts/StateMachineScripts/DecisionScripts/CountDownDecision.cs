﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Decision_", menuName = "Troop/Decisions/CountDown")]
public class CountDownDecision : Decision
{

    public float minTimer;
    public float maxTimer;
    private float timer;

    public override void OnStateEnterDecisionActions(StateController controller)
    {
        base.OnStateEnterDecisionActions(controller);

        if(minTimer > maxTimer)
        {
            minTimer = maxTimer;
        }
    }

    public override bool Decide(StateController controller)
    {        
        bool isCountDownFinished = controller.CheckIfCountDownElapsed(controller.GenerateRandomFloat(minTimer, maxTimer));
        return isCountDownFinished;
    }


}
