﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Decision_CheckAxe_", menuName = "Troop/Decisions/CheckAxe")]
public class CheckAxeDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        return controller.axeSensor.DetectedObjects.Count > 0;
    }
}
