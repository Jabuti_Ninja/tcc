﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Decision_CheckSensor_", menuName = "Troop/Decisions/CheckSensor")]
public class CheckSensorDecision : Decision
{
    public enum SensorSide {FRONT, BACK, RIGHT, LEFT };
    public SensorSide sensorSide;

    public override bool Decide(StateController controller)
    {
        //switch(sensorSide)
        //{
        //    case SensorSide.FRONT:
        //        return controller.sensorController.frontSensor.IsObstructed;                

        //    case SensorSide.BACK:
        //        return controller.sensorController.backSensor.IsObstructed;

        //    case SensorSide.RIGHT:
        //        return controller.sensorController.rightSensor.IsObstructed;

        //    case SensorSide.LEFT:
        //        return controller.sensorController.leftSensor.IsObstructed;   
        //}

        if (sensorSide == SensorSide.FRONT)
            return controller.sensorController.frontSensor.IsObstructed;

        else if (sensorSide == SensorSide.BACK)
            return controller.sensorController.backSensor.IsObstructed;

        else if (sensorSide == SensorSide.RIGHT)
            return controller.sensorController.rightSensor.IsObstructed;

        else if (sensorSide == SensorSide.LEFT)
            return controller.sensorController.leftSensor.IsObstructed;
        else return true;
    }
}
