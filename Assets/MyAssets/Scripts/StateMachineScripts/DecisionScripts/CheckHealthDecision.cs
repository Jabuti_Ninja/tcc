﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Decision_", menuName = "Troop/Decisions/CheckHealth")]
public class CheckHealthDecision : Decision
{
    public float hpToCheck;

    public override bool Decide(StateController controller)
    {
        Debug.Log("HP percentage: " + controller.hitPoints.currentPoints / (float)controller.hitPoints.basePoints + "/ HP to Check: " + hpToCheck / 100);
        return controller.hitPoints.currentPoints/ (float)controller.hitPoints.basePoints <= hpToCheck/100;
    }
}
