﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Decision_", menuName = "Troop/Decisions/CheckAnimationEnd")]
public class CheckAnimationEndDecision : Decision
{
    public override bool Decide(StateController controller)
    {        
        return CheckIfAttackIsFinished(controller);
    }

    private static bool CheckIfAttackIsFinished(StateController controller)
    {
        return !controller.animator.GetCurrentAnimatorStateInfo(0).IsName(controller.currentAnimation);
    }
}
