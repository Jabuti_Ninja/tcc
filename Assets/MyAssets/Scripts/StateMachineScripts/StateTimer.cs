﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateTimer : MonoBehaviour
{
    private StateController stateController;

    public State[] allowedStates;
    public State stateToChange;
    
    [HideInInspector] private float baseTimer = 20;
    private float timer;

    private void Start()
    {
        stateController = GetComponent<StateController>();
    }

    void Update()
    {
        if(CheckState())
            CheckTimer();
    }

    private bool CheckState()
    {
        foreach (var state in allowedStates)
        {
            if (stateController.currentState = state)
                return true;
        }

        timer = 0;
        return false;        
    }

    private void CheckTimer()
    {
        timer += Time.deltaTime;
        if (timer > baseTimer)
        {
            ChangeState();
        }
    }

    private void ChangeState()
    {
        stateController.currentState = stateToChange;
        timer = 0;
    }
}
