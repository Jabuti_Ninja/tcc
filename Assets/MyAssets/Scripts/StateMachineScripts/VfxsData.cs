﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VfxsData
{
    public string name;
    public ParticleSystem[] particleSystems;
    public bool isLoop = false;
}
