﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Troop/Actions/SummonEnemies", fileName = "Action_")]
public class SummonEnemiesAction : Action
{
    public int boitataAmount = 0;
    public int curupiraAmount = 0;
    public int mulaAmount = 0;

    private EnemySummoner enemySummoner;
    

    public override void OnStateEnterAction(StateController controller)
    {
        enemySummoner = controller.GetComponent<EnemySummoner>();

        SummonEnemies(0, boitataAmount);
        SummonEnemies(1, curupiraAmount);

        SummonEnemies(2, mulaAmount);


        //   for (int i = 0; i < mulaAmount; i++) //SummonMula
        //   {
        //      var enemy = enemySummoner.SummonEnemy(2);
        //      enemiesSummoned.Add(enemy);
        //   }
        //Debug.Log(enemySummoner.enemiesSummoned.Count);

        foreach (var enemy in enemySummoner.enemiesSummoned)
        {

            enemy.target = controller.target;
            enemy.Undead();
            enemy.StartCombat();
        }       

    }

    private void SummonEnemies(int index, int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            var enemy = enemySummoner.SummonEnemy(index);
            enemySummoner.enemiesSummoned.Add(enemy);
            Debug.Log("EnemySummoned");
        }
    }

}
