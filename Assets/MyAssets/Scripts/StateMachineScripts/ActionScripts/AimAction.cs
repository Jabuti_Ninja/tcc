﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/Aim")]
public class AimAction : Action
{
    public float aimSpeed = 15;
    public bool aimParticle;

    public override void Act(StateController controller)
    {
        LookAtPlayer(controller);
    }

    private void LookAtPlayer(StateController controller)
    {
        Vector3 direction = (controller.target.position - controller.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        controller.transform.rotation = Quaternion.Slerp(controller.transform.rotation, lookRotation, Time.deltaTime * aimSpeed);
        if(aimParticle)
        {
            if (controller.weaponParticle)
            {
                controller.weaponParticle.transform.rotation = Quaternion.Slerp(controller.transform.rotation, lookRotation, Time.deltaTime * aimSpeed);
            }
        }

    }
}
