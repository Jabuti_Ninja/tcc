﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/ActivateBossPuzzle", fileName = "Action_")]
public class ActivateBossPuzzleAction : Action
{
    public int puzzleIndex;

    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        controller.GetComponent<BossPuzzles>().puzzles[puzzleIndex].gameObject.SetActive(true);
    }
}
