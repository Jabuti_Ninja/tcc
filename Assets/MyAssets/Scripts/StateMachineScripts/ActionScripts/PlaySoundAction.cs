﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/PlaySound", fileName = "Action_")]
public class PlaySoundAction : Action
{
    public string soundPath;
    public float startDelay;

    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        controller.StartCoroutine(PlaySound(controller));        
    }

    public IEnumerator PlaySound(StateController controller)
    {
        yield return new WaitForSeconds(startDelay);
        FMODUnity.RuntimeManager.PlayOneShot(soundPath, controller.transform.position);
    }
}
