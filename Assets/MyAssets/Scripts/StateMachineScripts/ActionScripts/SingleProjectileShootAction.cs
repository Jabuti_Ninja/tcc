﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/SingleProjectileShoot")]
public class SingleProjectileShootAction : Action
{
    public int damage;
    public int shotPosToUse = 0;
    public bool useAllShotPos = false;
    public float delay = 0;

    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        controller.StartCoroutine(Fire(controller, delay));
    }

    public IEnumerator Fire(StateController controller, float delay)
    {
        yield return new WaitForSeconds(delay);
        if(useAllShotPos)
        {
            foreach (var eye in controller.eyes)
            {
                controller.enemyShooting.FireSingleProjectile(eye, damage, controller.troopData.attackForce);
            }
        }
        else
        {
            controller.enemyShooting.FireSingleProjectile(controller.eyes[shotPosToUse], damage, 0);
        }
    }
}
