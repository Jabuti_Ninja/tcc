﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/ParticleShoot")]
public class ParticleShootAction : Action
{
    private float nextFireTime;

    public bool useSecondaryParticle = false;
    public int secondaryParticleIndex = 0;
    public bool shootOnEnterOnly = false;

    public override void Act(StateController controller)
    {
        if(!shootOnEnterOnly)
            Fire(controller);
    }

    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        if(shootOnEnterOnly)
        {
            if (!useSecondaryParticle)
                controller.enemyShooting.FireSingleParticle(controller.weaponParticle);            
            else
                controller.enemyShooting.FireSingleParticle(controller.secondaryWeaponParticles[secondaryParticleIndex]);
        }
    }




    public void Fire(StateController controller)
    {
        if(!useSecondaryParticle)
            controller.enemyShooting.FireParticle(controller.weaponParticle, controller.myAttackRate);
        else
            controller.enemyShooting.FireParticle(controller.secondaryWeaponParticles[secondaryParticleIndex], controller.myAttackRate);
    }
}
