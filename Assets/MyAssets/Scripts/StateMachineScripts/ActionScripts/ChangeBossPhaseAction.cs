﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/ChangeBossPhase", fileName = "Action_ChangeBossPhase")]
public class ChangeBossPhaseAction : Action
{
    [Range(1,3)]public int phaseToChange = 1;
    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        controller.GetComponent<BossPuzzles>().currentBossPhase = phaseToChange;
    }
}
