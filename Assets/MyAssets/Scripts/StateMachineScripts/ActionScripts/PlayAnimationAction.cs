﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Action_" , menuName = "Troop/Actions/PlayAnimation")]
public class PlayAnimationAction : Action
{

    public string animationName;
    public string animationTrigger;

    public override void OnStateEnterAction(StateController controller)
    {
        if(!controller.animator.GetCurrentAnimatorStateInfo(0).IsName(animationName))
        {
            controller.animator.ResetTrigger(controller.currentAnimationTrigger);
            controller.currentAnimation = animationName;
            controller.currentAnimationTrigger = animationTrigger;
            controller.animator.SetTrigger(animationTrigger);
        }
    }
}
