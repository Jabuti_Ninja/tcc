﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_RemovePlayerPower", menuName = "Troop/Actions/RemovePlayerPower")]

public class RemovePlayerPowerAction : Action
{
    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        PlayerActor.playerInstance.ResetToCopyPower();
    }
}
