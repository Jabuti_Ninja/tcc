﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(menuName = "Troop/Actions/Dash")]
public class DashAction : Action
{
    public Vector3 destination;
    public float dashDuration;

    public override void OnStateEnterAction(StateController controller)
    {
        //Dash(controller);
    }

    public override void OnStateExitAction(StateController controller)
    {
        base.OnStateExitAction(controller);
        controller.navMeshAgent.isStopped = false;
    }


    private void Dash(StateController controller)
    {
        controller.rb.isKinematic = true;
        controller.navMeshAgent.isStopped = true;
        controller.rb.DOMove(controller.transform.position + destination, dashDuration);
        //controller.soundBank.PlaySoundBankAudio(controller.soundBank.dash, GlobalAudioPlayer.Instance.audioSource, false);
    }

}
