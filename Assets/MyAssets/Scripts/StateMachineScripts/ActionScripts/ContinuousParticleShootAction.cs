﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/ContinuousParticleShoot")]
public class ContinuousParticleShootAction : Action
{
    public float shootCooldown = 3;
    public float shootDuration = 7;
    private bool onCooldown = false;

    public override void Act(StateController controller)
    {
        base.Act(controller);

        var loopParticle = controller.weaponParticle.main;
        
        if(!onCooldown)
        {
            if (!controller.enemyShooting.CheckTimerContinuous(shootDuration))
            {
                loopParticle.loop = true;
                if(!controller.weaponParticle.isPlaying)
                {
                    controller.weaponParticle.Play();
                    //controller.soundBank.PlaySoundBankAudio(controller.soundBank.primaryAttack, GlobalAudioPlayer.Instance.audioSource, true);
                    if (controller.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>())
                        controller.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>().Play();

                }
            }
            else
                onCooldown = true;
        }
        else
        {
            if (!controller.enemyShooting.CheckTimerContinuous(shootCooldown))
            {
                loopParticle.loop = false;
                //controller.audioSource.loop = false;
                //controller.audioSource.Stop();
                if (controller.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>())
                    controller.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>().Stop();

            }
            else
                onCooldown = false;
        }
    }

    public override void OnStateExitAction(StateController controller)
    {
        base.OnStateExitAction(controller);
        var loopParticle = controller.weaponParticle.main;
        loopParticle.loop = false;
        if(controller.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>())
        controller.weaponParticle.GetComponent<FMODUnity.StudioEventEmitter>().Stop();
        //controller.audioSource.loop = false;
        //controller.audioSource.Stop();
    }
}
