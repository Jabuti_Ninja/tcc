﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/Death")]
public class DeathAction : Action
{
    public float deathDelay = 3;
    public override void OnStateEnterAction(StateController controller)
    {
        //controller.ragdoller.DoRagdoll(true);
        if(controller.isActiveAndEnabled)
            controller.StartCoroutine(controller.SelfDisable(deathDelay));
    }
}
