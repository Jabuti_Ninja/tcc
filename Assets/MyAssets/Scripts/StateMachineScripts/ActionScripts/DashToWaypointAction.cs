﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[CreateAssetMenu(menuName = "Troop/Actions/DashToWayoint")]
public class DashToWaypointAction : Action
{
    public float dashDuration = .75f;

    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);

        if(controller.activeWaypoints.Count != 0)
        {
            controller.nextWaypoint = controller.activeWaypoints[Random.Range(0, controller.activeWaypoints.Count)];
            controller.unactiveWaypoints.Add(controller.nextWaypoint);
            controller.activeWaypoints.Remove(controller.nextWaypoint);
        }
        else
        {
            foreach (var item in controller.unactiveWaypoints)
            {
                controller.activeWaypoints.Add(item);
            }
            controller.unactiveWaypoints.Clear();
            controller.nextWaypoint = controller.activeWaypoints[Random.Range(0, controller.activeWaypoints.Count)];
            controller.unactiveWaypoints.Add(controller.nextWaypoint);
            controller.activeWaypoints.Remove(controller.nextWaypoint);
        }

        bool activeTween = controller.currentTween.IsActive();

        if (!activeTween)
        {
            controller.currentTween = controller.rb.DOMove(controller.nextWaypoint.position, dashDuration);
        }
    }
}
