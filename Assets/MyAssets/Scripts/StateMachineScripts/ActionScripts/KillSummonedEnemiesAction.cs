﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/KillSummonedEnemies", fileName = "Action_")]
public class KillSummonedEnemiesAction : Action
{
    private EnemySummoner enemySummoner;

    public override void OnStateEnterAction(StateController controller)
    {
        enemySummoner = controller.GetComponent<EnemySummoner>();

        foreach (var enemy in enemySummoner.enemiesSummoned)
        {
            enemy.ChangeState(enemy.deadState);
        }

        enemySummoner.enemiesSummoned.Clear();
    }
}
