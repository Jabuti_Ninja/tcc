﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/ChangeLayer", fileName = "Action_ChangeLayer")]
public class ChangeLayerAction : Action
{
    public int layerIndex;

    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        var colParent = controller.GetComponent<BossPuzzles>().collidersParent;
        colParent.layer = layerIndex;
    }
}
