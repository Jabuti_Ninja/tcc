﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/SingleParabolaShoot" , fileName = "Action_")]

public class SingleParabolaShootAction : Action
{
    public int damage = 15;
    public float parabolaHeight = 5;
    public float parabolaDuration = 3;

    public float startDelay = 0;

    public int shotPosToUse = 0;
    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        controller.StartCoroutine(ShootParabolaBullet(controller, startDelay));
    }

    private IEnumerator ShootParabolaBullet(StateController controller, float delay)
    {
        yield return new WaitForSeconds(delay);
        controller.enemyShooting.FireParabolaProjectile(controller.eyes[shotPosToUse], damage, controller.target, parabolaHeight, parabolaDuration);

    }
}
