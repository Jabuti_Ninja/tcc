﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/ChangeFireRateAction", fileName = "Action_ChangeFireRate_")]
public class ChangeFireRateAction : Action
{
    public float newAttackRate = 0;

    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        controller.myAttackRate = newAttackRate;
    }
}
