﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/StopMoving")]
public class StopMovingAction : Action
{
    public bool lockMovement = true;
    public bool lockRotation = true;
    public bool allowRigidBody = false;

    public override void Act(StateController controller)
    {
        SetupMovement(controller, lockMovement, lockRotation);
    }

    public override void OnStateEnterAction(StateController controller)
    {
        base.OnStateEnterAction(controller);
        if(allowRigidBody)
        {
            controller.rb.isKinematic = false;
            controller.navMeshAgent.isStopped = true;
        }
    }

    public override void OnStateExitAction(StateController controller)
    {
        base.OnStateExitAction(controller);
        //controller.rb.isKinematic = true;
        SetupMovement(controller, false, false);
    }

    private void SetupMovement(StateController controller, bool lockMov, bool lockRot)
    {
        if(controller.isActiveAndEnabled)
            controller.navMeshAgent.isStopped = lockMov;

        if(lockMov)
        {
            controller.transform.position = controller.transform.position;
        }

        if (lockRot)
            controller.transform.rotation = controller.transform.rotation;
    }

    
}
