﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TroopData_", menuName = "Troop/ TroopData")]
public class TroopData : ScriptableObject
{
    [Range(1, 99)] public int powerId;
    public string target;

    public int baseHP;

    [Header("Attack Settings")]

    public int attackDamage;
    public float attackRate;
    public float attackForce;

    [Header("Movement Settings")]
    public float movementSpeed;

    public float closeAreaRange = 3;
    public float farAreaRange;
    public float dashForce;

   
}
