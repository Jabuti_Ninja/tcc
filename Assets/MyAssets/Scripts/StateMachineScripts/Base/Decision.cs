﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Decision : ScriptableObject
{
    //public float decisionSuccessDelay;

    public virtual void OnStateEnterDecisionActions(StateController controller)
    {

    }
    public abstract bool Decide(StateController controller);


}
