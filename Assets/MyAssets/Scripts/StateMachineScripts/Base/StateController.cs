﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
using UnityEngine.Animations.Rigging;
using SensorToolkit;

public class StateController : Actor
{

    public State currentState;
    public State remainState;
    public State startingState;
    public State outOfCombatState;
    public State deadState;
    public State hitstunState;

    public bool dead;


    public TroopData troopData;
    [HideInInspector] public float myAttackRate;

    public Transform[] eyes = new Transform[1];
    public float freezeTime = 1f;

    public RangeSensor axeSensor;
    [HideInInspector] public SensorController sensorController;
    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public EnemyShooting enemyShooting;
    // [HideInInspector]
    public Animator animator;
    //  [HideInInspector] public TroopDeath troopDeath;

    [HideInInspector] public Transform chaseTarget;
    [HideInInspector] public float stateTimeElapsed;
    [HideInInspector] public float thrustSpeed = 5;
    public Transform target;

    [HideInInspector] public string currentAnimation;
    [HideInInspector] public string currentAnimationTrigger;

    [HideInInspector] public bool endForce = false;

    public List<Transform> activeWaypoints;
    public List<Transform> unactiveWaypoints;
    [HideInInspector] public Transform nextWaypoint;


    [SerializeField] private bool aiActive = true;
    [SerializeField] private GameObject armIK;

    [SerializeField] private ParticleSystem deathVFX;

    public Tween currentTween;

    protected override void Awake()
    {
        base.Awake();
        enemyShooting = GetComponent<EnemyShooting>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = troopData.movementSpeed;
        rb = GetComponent<Rigidbody>();
        sensorController = GetComponentInChildren<SensorController>();


        // animator = GetComponent<Animator>();
        // if (!animator)
        //     animator.GetComponentInChildren<Animator>();

        hitPoints = GetComponent<HitPoints>();
        hitPoints.basePoints = troopData.baseHP;
        //      troopDeath = GetComponent<TroopDeath>();
        currentState = outOfCombatState;
    }

    protected override void Start()
    {
        base.Start();
        if (weaponParticle)
            weaponParticle.GetComponent<ParticleCollisionListener>().damage = troopData.attackDamage;
        myAttackRate = troopData.attackRate;
        OnEnable();
        //audioSource = GlobalAudioPlayer.Instance.audioSource;
    }

    private void OnEnable()
    {
        StartCoroutine(UpdateStates());
        hitPoints.onDamage += StartCombat;
        //hitPoints.onDamage += PlayTakeHitSFX;
    }

    private void OnDisable()
    {
        hitPoints.onDamage -= StartCombat;
        //hitPoints.onDamage -= PlayTakeHitSFX;
    }

    protected override void Update()
    {
        base.Update();
        //   troopDeath.CheckHP(hitPoints);      
        if (hitPoints.currentPoints <= 0)
        {
            Dead();
        }
    }

    public IEnumerator UpdateStates()
    {
        while (true)
        {
            if (!aiActive)
                yield return new WaitForEndOfFrame();
            else
            {
                currentState.UpdateState(this);
                yield return new WaitForEndOfFrame();
            }

        }

    }

    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            ChangeState(nextState);

            currentState = nextState;


            OnExitState();
        }
    }

    private void OnExitState()
    {
        ResetCountDown();
    }

    public void ChangeState(State stateToChange)
    {
        currentState.OnStateExit(this);
        currentState = stateToChange;
        currentState.OnStateEnter(this);
    }

    public void StartCombat()
    {
        if (currentState == outOfCombatState || currentState == deadState && !dead)
        {
            ChangeState(startingState);
            if (armIK)
                armIK.GetComponent<IRigConstraint>().weight = 1;
        }

        if (hitPoints.slider != null)
        {
            hitPoints.slider.gameObject.SetActive(true);
        }
    }

    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return stateTimeElapsed >= duration;
    }

    private void ResetCountDown()
    {
        stateTimeElapsed = 0;
    }

    public void StartForce()
    {
        endForce = false;
    }

    public void EndForce()
    {
        endForce = true;
    }

    public void ShootProjectiles()
    {
        enemyShooting.FireProjectile(eyes, myAttackRate, troopData.attackDamage, troopData.attackForce);
    }

    public void Hitstun()
    {
        ChangeState(hitstunState);
    }

    public void Dead()
    {
        SetHpSlider(false);
        if (currentState != deadState)
            ChangeState(deadState);
        if (GetComponentInParent<CombatStarter>() && !dead)
        {
            dead = true;
            GetComponentInParent<CombatStarter>().CheckOpenArena();
        }

        if (!dead)
        {
            dead = true;
        }
        if (armIK)
            armIK.GetComponent<IRigConstraint>().weight = 0;
    }
    public void Undead()
    {
        SetHpSlider(true);

        if (dead)
        {
            dead = false;
        }
        if (armIK)
            armIK.GetComponent<IRigConstraint>().weight = 1;
        hitPoints.currentPoints = troopData.baseHP;
    }



    private void OnDrawGizmos()
    {
        Gizmos.color = currentState.sceneGizmoColor;
        Gizmos.DrawWireSphere(transform.position, troopData.closeAreaRange);
        Gizmos.DrawWireSphere(transform.position, troopData.farAreaRange);
    }

    public void SetHpSlider(bool valueToSet)
    {
        if (hitPoints.slider)
            hitPoints.slider.gameObject.SetActive(valueToSet);
    }


    public IEnumerator SelfDisable(float delay)
    {
        yield return new WaitForSeconds(delay);

        deathVFX.Play();
        yield return new WaitForSeconds(.12f);
        //soundBank.PlaySoundBankAudio(soundBank.death, GlobalAudioPlayer.Instance.audioSource, false);
        deathVFX.transform.parent = null;
        gameObject.SetActive(false);
    }

    public float GenerateRandomFloat(float minValue, float maxValue)
    {
        return Random.Range(minValue, maxValue);
    }

    //public void PlayTakeHitSFX()
    //{
    //    soundBank.PlaySoundBankAudio(soundBank.takeHit, GlobalAudioPlayer.Instance.audioSource, false);
    //}
}
