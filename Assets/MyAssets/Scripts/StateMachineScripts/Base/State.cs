﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "State_", menuName = "Troop/State")]

public class State : ScriptableObject
{
    public Action[] actions = new Action[1];
    public Transition[] transitions = new Transition[1];
    public Color sceneGizmoColor = Color.grey;


    public void UpdateState(StateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    public void OnStateEnter(StateController controller) //Chamar depois da troca de estado
    {
        foreach (var action in actions)
        {
            action.OnStateEnterAction(controller);
        }

        foreach (var transition in transitions)
        {
            transition.decision.OnStateEnterDecisionActions(controller);
        }
    }

    public void OnStateExit(StateController controller) //Chamar antes da troca de estado
    {        
        foreach (var action in actions)
        {            
            action.OnStateExitAction(controller);
        }
    }

    public void DoActions(StateController controller)
    {
        for (int i = 0; i < actions.Length; i++)
        {
            actions[i]?.Act(controller);
        }
    }

    public void CheckTransitions(StateController controller)
    {
        for (int i = 0; i < transitions.Length; i++)
        {
            bool decisionSucceeded = transitions[i].decision.Decide(controller);

            if (decisionSucceeded)
            {
                controller.TransitionToState(transitions[i].trueState[Random.Range(0, transitions[i].trueState.Length)]);
            }
            else
            {
                controller.TransitionToState(transitions[i].falseState[Random.Range(0, transitions[i].falseState.Length)]);
            }
        }
    }
}
