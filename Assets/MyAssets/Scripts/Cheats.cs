﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Cheats : MonoBehaviour
{
    private PlayerActor player;

    [SerializeField] private GameObject invulnerableFeedback;

    private void Start()
    {
        player = GetComponent<PlayerActor>();
    }

    public void InvunerableCheat(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            player.hitPoints.invulnerable = !player.hitPoints.invulnerable;
            invulnerableFeedback.SetActive(player.hitPoints.invulnerable);
        }
    }

    public void GetAxe(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            player.copyObject.gameObject.SetActive(true);
            player.released = true;
            player.controller.released = true;
        }
    }
}
