﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HordeManagerScript : MonoBehaviour
{
    public Horde[] hordes;

    private int currentHordeID;
    private bool started;

    private Collider col;

    private void Start()
    {
        col = GetComponent<Collider>();
    }

    private void Update()
    {
        CheckHordeSpawn();
        CheckEnemyHordeDeath();
    }

    private void CheckHordeSpawn()
    {
        if (hordes[currentHordeID].hordeEnemies.Count <= 0)
        {
            if (hordes.Length > currentHordeID + 1)
            {
                currentHordeID++;
                StartCoroutine(SpawnNewHorde(currentHordeID, hordes[currentHordeID].delayToSpawn, hordes[currentHordeID].startCombatDelay));
            }
            else
                CheckDisable();

        }
    }

    private IEnumerator SpawnNewHorde(int hordeToSpawn, float delayToSpawn, float startCombatDelay)
    {
        yield return new WaitForSeconds(delayToSpawn);
        FMODUnity.RuntimeManager.PlayOneShot("event:/Enemy/Enemy_spawn", transform.position);
        for (int i = 0; i < hordes[hordeToSpawn].hordeEnemies.Count; i++)
        {
            hordes[hordeToSpawn].hordeEnemies[i].gameObject.SetActive(true);
        }
        StartCoroutine(StartCombatWithDelay(hordeToSpawn, startCombatDelay));

    }

    private IEnumerator StartCombatWithDelay(int hordeToSpawn, float startCombatDelay)
    {
        yield return new WaitForSeconds(startCombatDelay);
        for (int i = 0; i < hordes[hordeToSpawn].hordeEnemies.Count; i++)
        {
            hordes[hordeToSpawn].hordeEnemies[i].StartCombat();
        }
    }


    private void CheckEnemyHordeDeath()
    {
        for (int i = 0; i < hordes[currentHordeID].hordeEnemies.Count; i++)
        {
            if (hordes[currentHordeID].hordeEnemies[i].dead)
            {
                hordes[currentHordeID].hordeEnemies.RemoveAt(i);
            }
        }
    }

    private void CheckDisable()
    {
        if (hordes[currentHordeID].hordeEnemies.Count <= 0)
        {
            StartCoroutine(SelfDisable());
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!started && other.tag == "Player" || !started && other.tag == "CopyObject")
        {
            StartCoroutine(SpawnNewHorde(0, hordes[0].delayToSpawn, hordes[0].startCombatDelay));
            started = true;
            LevelMusicSetter.battleMusicCount++;
            col.enabled = false;
        }
    }

    IEnumerator SelfDisable()
    {
        yield return new WaitForSeconds(3f);
        LevelMusicSetter.battleMusicCount--;
        gameObject.SetActive(false);
    }
}

[System.Serializable]
public class Horde
{
    public List<StateController> hordeEnemies = new List<StateController>();
    public float delayToSpawn;
    public float startCombatDelay;
}
