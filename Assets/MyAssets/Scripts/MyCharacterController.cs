﻿using ECM.Common;
using ECM.Controllers;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

namespace ECM.Walkthrough.MovementRelativeToCamera
{
    /// <summary>
    /// Custom character controller. This shows how make the character move relative to MainCamera view direction.
    /// </summary>

    public class MyCharacterController : BaseCharacterController
    {

        public Camera mainCamera;
        public Transform camFollowTarget;

        //[HideInInspector]public PlayerInputActions inputActions;
        [HideInInspector] public PlayerInput inputActions;
        public CinemachineFreeLook cinemachineFreeLook;
        //private AudioSource audioSource;

        public delegate void OnJump();
        public event OnJump onJump;


        public delegate void OnWalk();
        public event OnJump onWalk;

        [HideInInspector] public bool interact;


        // Bool adicionada para fazer a cutscene
        public bool released = true;

        //private void Start()
        //{
        //    audioSource = GetComponent<AudioSource>();
        //}


        public override void Awake()
        {
            base.Awake();
            inputActions = new PlayerInput();
            inputActions.Enable();

        }
        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();

        }

        protected override void Animate()
        {
            if (released && Time.deltaTime > 0)
            {
                animator.SetFloat("runX", inputActions.Player.Move.ReadValue<Vector2>().x);
                animator.SetFloat("runY", inputActions.Player.Move.ReadValue<Vector2>().y);

                if (inputActions.Player.Move.ReadValue<Vector2>().x != 0 || inputActions.Player.Move.ReadValue<Vector2>().y != 0)
                    onWalk?.Invoke();
            }
            else
            {
                animator.SetFloat("runX", 0);
                animator.SetFloat("runY", 0);
            }



            animator.SetBool("isGrounded", isGrounded);
        }

        protected override void UpdateRotation()
        {
            if (released)
            {
                RotateTowards(camFollowTarget.forward, true);

            }


        }

        protected override void HandleInput()
        {
            if (released)
            {
                // Toggle pause / resume.
                // By default, will restore character's velocity on resume (eg: restoreVelocityOnResume = true)

                //if (Input.GetKeyDown(KeyCode.P))
                //    pause = !pause;

                //// Handle user input

                //jump = inputActions.Player.Jump.ReadValue<bool>();

                //crouch = inputActions.Player.Crouch.ReadValue<bool>();

                moveDirection = new Vector3
                {
                    x = inputActions.Player.Move.ReadValue<Vector2>().x,
                    y = 0.0f,
                    z = inputActions.Player.Move.ReadValue<Vector2>().y
                };

                //Vector3 newSpeed = new Vector3();
                //newSpeed.x = Mathf.Lerp(moveDirection.x, inputActions.Player.Move.ReadValue<Vector2>().x, movementLerp);
                //newSpeed.z = Mathf.Lerp(moveDirection.z, inputActions.Player.Move.ReadValue<Vector2>().y, movementLerp);
                //moveDirection = newSpeed;

                // Transform the given moveDirection to be relative to the main camera's view direction.
                // Here we use the included extension .relativeTo...


                if (mainCamera != null)
                {
                    if (!PlayerActor.playerInstance.isDead)
                        moveDirection = moveDirection.relativeTo(mainCamera.transform);
                    else
                        moveDirection = Vector3.zero;
                }

            }

        }

        public void HandleJumpInput(InputAction.CallbackContext context)
        {
            if (released)
            {
                if (context.performed)
                {
                    jump = true;
                    animator.SetTrigger("jump");
                    onJump?.Invoke();

                }
                else if (context.canceled)
                    jump = false;
            }


        }

        public void HandleInteractInput(InputAction.CallbackContext context)
        {
            if (context.performed)
            {
                interact = true;
            }
            else if (context.canceled)
                interact = false;
        }

    }
}
