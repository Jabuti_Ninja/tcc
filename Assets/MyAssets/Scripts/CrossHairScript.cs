﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CrossHairScript : MonoBehaviour
{
    [SerializeField] private GameObject[] crossHairs;
    [SerializeField] private PlayerActor playerActor;

    private Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        for (int i = 0; i < crossHairs.Length; i++)
        {
            if (i == playerActor.currentPower.id)
                crossHairs[i].SetActive(true);
            else
                crossHairs[i].SetActive(false);

        }
    }

    public void ShootAnim(InputAction.CallbackContext context)
    {
        if (context.performed && playerActor.released && Time.timeScale > 0 && playerActor.copyObject.state == CopyObject.State.InHand)
        {
            if(playerActor.currentPower.id == 2)
            {
                anim.SetBool("boitata_active", true);
                anim.SetTrigger("boitata_open");

            }
            else
            {
                anim.SetBool("boitata_active", false);
                anim.SetTrigger("shoot");
            }
        }

        if(context.canceled && playerActor.currentPower.id == 2)
            anim.SetTrigger("boitata_close");

    }
}
