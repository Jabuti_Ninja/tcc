﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public enum DissolveType
{
    ENERGY_WALL,
    PLAYER_CUTSCENE
}

public class Dissolve : MonoBehaviour
{
    [SerializeField] private MeshRenderer[] renderers;
    [SerializeField] private SkinnedMeshRenderer[] skinnedRenderers;
    [Range(0, 1)] public float dissolve = 0f;
    [Range(0, 100)] public float emission = 0f;
    public bool animate;
    public DissolveType dissolveType = DissolveType.ENERGY_WALL;

    void Update()
    {
        if (animate)
        {
            switch (dissolveType)
            {
                case DissolveType.ENERGY_WALL:
                    SimpleDissolve();
                    break;

                case DissolveType.PLAYER_CUTSCENE:
                    EmissionDissolve();
                    break;
            }
        }          
    }

    void SimpleDissolve()
    {
        foreach (MeshRenderer renderer in renderers)
            renderer.material.SetFloat("_Dissolve", dissolve);
    }

    void EmissionDissolve()
    {
        foreach (SkinnedMeshRenderer skinnedMesh in skinnedRenderers)
        {
            skinnedMesh.sharedMaterial.SetFloat("_EmissionIntensity", emission);
            skinnedMesh.sharedMaterial.SetFloat("_Dissolve", dissolve);
        }
    }
}
